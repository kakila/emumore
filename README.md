## EmuMore's Repository

Welcome to the [EmuMore project](https://kakila.gitlab.io/emumore/about) repository.
Here you will find the source code for everything used in the project.

The source code is released under a the terms of the [GPLv3](http://www.gnu.org/licenses/) or later, and the wiki's contents under the terms of the [Creative Commons Attribution-ShareAlike 4.0 International Public License](https://creativecommons.org/licenses/by-sa/4.0/deed.en) or later.
You are free to use the material as long as you respect the licenses.
For information on how to cite this website and other EmuMore material, check the [wiki](https://gitlab.com/kakila/emumore/wiki/Home.wiki).

### Contributors
* Juan Pablo Carbajal
* Jörg Rieckermann
* Kris Villez

### Bugs, suggestions, requests

Please use the [issue tracker](https://gitlab.com/kakila/emumore/issues) for any bugs you discover or to send us suggestions and requests.

## About the project

EmuMore is a collaboration project funded by [Eawag](www.eawag.ch) and [HSR University of Applied Sciences Rapperswil](www.hsr.ch).

The project aims at provinding tools for the creation of surrogate models of detailed simulators for particular applications, e.g. realtime control, system identification or model calibration, design optiimzation, etc.

Surrogate models can be many times faster than the corresponding simulator, since to find a good solution to an specific problem we might not need all the flexibility of the simulator.

Read more at the [Project's webpage](https://kakila.bitbucket.io/emumore/about).
