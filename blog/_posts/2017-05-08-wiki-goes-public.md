---
layout: post
title: Wiki goes Public!
description: "Wiki content is now available to everybody"
tags: [wiki]
comments: true
date: 2017-05-08T10:19:36+02:00
link: https://bitbucket.org/KaKiLa/emumore/wiki/Home
image:
  feature: feature_news_green.jpg
  credit:
  creditlink:
---

From now on everybody is able to read the content in our wiki.
**Be warned**: the wiki content is *crazy*.
