---
layout: post
title: Repository goes Public!
description: "The contents of the repository are now available to everybody"
tags: [code]
comments: true
date: 2017-06-01T17:20:00+02:00
link: https://bitbucket.org/KaKiLa/emumore/src
image:
  feature: feature_news_green.jpg
  credit:
  creditlink:
---

From now on everybody is able to read and fork the content in our repostiory.
**Be warned**: it is in pre-alpha so mind your steps!
