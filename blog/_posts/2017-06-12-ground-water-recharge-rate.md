---
layout: post
title: Groundwater Recharge Rate
description: New case study with dataset, groundwater recharge rate.
tags: [dataset]
image:
  feature: feature_dataset.jpg
  credit:
  creditlink:
comments: true
mathjax: true
date: 2017-06-12T10:24:31+02:00
---

After a fruitful meeting with [Christian Möck](http://www.eawag.ch/en/aboutus/portrait/organisation/staff/profile/christian-moeck/show/) from the Department of [Water Resources & Drinking Water](http://www.eawag.ch/en/department/wut/) at Eawag, we agreed to collaborate on building an emulator for a physics based simulator of [Groundwater recharge rates](https://en.wikipedia.org/wiki/Groundwater_recharge).

<figure class="center">
  <img src="https://upload.wikimedia.org/wikipedia/commons/8/80/Surface_water_cycle.svg" alt="Surface water cycle" align="middle">
  <figcaption>Credit: M. W. Toews under <a src="http://creativecommons.org/licenses/by/4.0">CCBY 4.0</a> via Wikimedia Commons.</figcaption>
</figure>

<!-- more -->

Groundwater recharge is the primary hydrologic process through which water enters the soil and refills aquifers (so important!).
Understanding this process is fundamental to the sustainable management of groundwater since the recharge magnitude directly affects the amount of water that can be extracted from aquifers.

Therefore there has been many developments in the area of recharge modeling ([SWAP](http://www.swap.alterra.nl/), [MODFLOW](https://water.usgs.gov/software/lists/groundwater), etc) and a major issue is the calibration of these models.
In this collaboration we will build an emulator for an in-house simulator with the aim of running a full-fledged calibration using [Monte Carlo methods](https://en.wikipedia.org/wiki/Monte_Carlo_method).

One of the fundamental models for the dynamics of recharge is the [Richards equation](https://en.wikipedia.org/wiki/Richards_equation)

$$ \frac{\partial \theta}{\partial t}= \frac{\partial}{\partial z} \left[ K(\theta) \left (\frac{\partial h}{\partial z} + 1 \right) \right] $$

where \\(K\\) is the hydraulic conductivity (the ease with which water can move through pore spaces or fractures), \\(h\\) is the potential energy of the water (matric head) induced by capillary action, \\(z\\) is the elevation above a vertical reference height (called datum in geodesy), \\(\theta\\) is the volumetric water content, and \\(t\\) is time.

We will be playing a lot with this equation in the future!
