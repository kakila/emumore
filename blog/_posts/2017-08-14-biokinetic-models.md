---
layout: post
title: Biokinetic Models
modified:
description: New case study with dataset, bioreactors.
tags: [dataset]
image:
  feature: feature_dataset.jpg
  credit:
  creditlink:
comments: true
mathjax: true
date: 2017-08-14T14:03:15+02:00
---

New process technologies that enable the shift from conventional biological 
wastewater treatment processes to resource recovery systems 
are matched by an increasing demand for predictive capabilities. 
Mathematical models are excellent tools to meet this demand, but 
they runtime still hinders parameter indetification and real-time control.

<!-- more -->

In collaboration with [Kris villez](http://www.eawag.ch/en/aboutus/portrait/organisation/staff/profile/kris-villez/show/) 
from the Department Process Engineering at Eawag, we are developing 
emulators to accelerate the different modules that compose the simulators
of Nitrification bioreactors.

One important step in the simulation of these systems is the calulation of
the pH based on the state of the reactor[^Masic2017][^FloresAlsina2015].
The pH is estimated by a root finding process that could be accelerated
inducing an overall speed up of the simulator.

We have been working on simplified versions of the root finding module.
The first test was in [2-dimensions](https://bitbucket.org/KaKiLa/emumore/wiki/Bioreactor_CS.rst#!case-study-bioreactor) 
and a later one in [4-dimensions](https://bitbucket.org/KaKiLa/emumore/wiki/Bioreactor_CS_2.rst#!case-study-bioreactor-4d-input).
The results are promising and next we will tackle the 12-dimensional case.

<figure class="center">
  <img src="https://bitbucket.org/KaKiLa/emumore/wiki/img/bioreactor4d_GP.png" alt="pH as function of 4 inputs" align="middle">
  <figcaption>Credit: Juan Pablo Carbajal under <a src="http://creativecommons.org/licenses/by/4.0">CCBY 4.0</a> via Emumore's Wiki.</figcaption>
</figure>

We will make the final dataset publicly available.

<a href="#" class="btn btn-info">Stay tuned!</a>

# References

[^Masic2017]: Masic, A., Srinivasan, S., Billeter, J., Bonvin, D., & Villez, K. (2017). Identification of Biokinetic Models using the Concept of Extents. Environmental Science & Technology. http://doi.org/10.1021/acs.est.7b00250

[^FloresAlsina2015]: Flores-Alsina, X., Kazadi Mbamba, C., Solon, K., Vrecko, D., Tait, S., Batstone, D. J., Jeppsson, U., Gernaey, K. V. (2015). A plant-wide aqueous phase chemistry module describing pH variations and ion speciation/pairing in wastewater treatment process models. Water Research, 85, 255–265. http://doi.org/10.1016/j.watres.2015.07.014

