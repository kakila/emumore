---
layout: post
title: Emulation Talk at ZHAW Winterthur, Switzerland
tags: [talk]
image:
  feature: feature_news_green.jpg
  credit:
  creditlink:
comments: true
date: 2017-08-14T13:37:22+02:00
link: https://sites.google.com/site/juanpicarbajal/public-talks/speedingupnumericalsimulatorswithemulators
---

We will presenting some of our work at the 
2nd European COST Conference on Mathematics for Industry in Switzerland, 
Artificial Intelligence in Industry and Finance.
