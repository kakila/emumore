---
layout: post
title: Wiki renders math
description: "Wiki did not redner math"
tags: [wiki]
comments: true
link: https://bitbucket.org/KaKiLa/emumore/wiki/Home
image:
  feature: feature_news_green.jpg
  credit:
  creditlink:
date: 2017-09-20T08:10:34+02:00
---

If you have peeked into our [wiki](https://bitbucket.org/KaKiLa/emumore/wiki/Home) you might have noticed that MathJax was not rendering, sorry for that! 
This was due to a bug in the BitBucket platform that now is fixed: kudos for the Atlassian team!

Now you should get rendered math even if you do not have a BitBucket account, [give it a try](https://bitbucket.org/KaKiLa/emumore/wiki/adimensional_shallow_water.rst).
