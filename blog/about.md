---
layout: page
title: About the project
permalink: /about/
share: true
---

Overview

[EmuMore](https://bitbucket.org/KaKiLa/emumore) is a collaboration project funded by [Eawag](http://www.eawag.ch) and [HSR University of Applied Sciences Rapperswil](http://www.hsr.ch).

The project aims at providing tools for the creation of surrogate models of detailed simulators for particular applications, e.g. realtime control, system identification or model calibration, design optimization, etc.

Surrogate models can be many times faster than the corresponding simulator, since to find a good solution to an specific problem we might not need all the flexibility of the simulator.

The contents of this website are released under a [Creative Commons Attribution-ShareAlike 4.0 International Public License](https://creativecommons.org/licenses/by-sa/4.0/deed.en) or later.
Please read the license deed to understand how to use the contents of this page.
For information about how to cite this website and other EmuMore material, check the [wiki](https://bitbucket.com/KaKiLa/emumore/wiki/Home.wiki).

## On this page
{: .no_toc}
- TOC
{:toc}

# The problem
As societies become increasingly connected, the amount of monitoring data grows beyond the capacity of water managers to make sensible use of it[^Sedlak14].
In this scenario, management requires good computer models to harvest the data and support decision making [^Reichert05], which leads scientists and practitioners to apply nonlinear, highly parameterized complex models to study these processes.
Unfortunately, this complexity brings about long computation times, which prohibits rigorous sensitivity analysis, optimization, statistical inference, and also real-time control.

The direct consequence of time consuming simulators is that researchers work less efficiently and that results can be questionable because parameter values are chosen on an ad-hoc basis.
In urban water practice, they also preclude the intelligent operation of urban infrastructure through state-of-the-art control methods, such as model-based predictive control.

# The solution
There are four approaches to deal with these problems: **i)** discard systems analysis and intelligent control strategies, **ii)** work only with utterly simplified models, **iii)** use high-performance computing (HPC), and **iv)** construct fast surrogate models. The first two are inefficient and the third, using HPC, often requires considerable investment in HPC know-how and IT equipment.
It often also requires re-programming simulation software, which is not always the best strategy for all projects and problems.
Therefore, constructing fast surrogate models, so-called _emulators_, to speed up slow simulators is very attractive.
It does not require a huge investment in new hardware and software, and the same tool can be used to solve very different problems.

However, although we have obtained promising results for the emulation of receiving waters [^Reichert11] and urban drainage systems [^Carbajal16], we find that the emulation tools we have developed so far are not yet ready to tackle nonlinear problems.
Also, it is unclear in how far they can improve the operation of urban water systems and how they can be used to fuse the information from wireless sensor networks, with potentially hundreds of sensors.

# Goals
1. Improve and develop emulation methods

    We will continue improving our Gaussian Processes based emulation methods and
    incorporate algorithms from Reduced Basis Methods[^Quarteroni16].

2. Demonstrate the usefulness of emulators for urban wastewater systems

    We will provide many realistic case studies where emulation give an edge in
    System Identification (a.k.a. model calibration), Design Optimization, and
    Model Predictive Control.

3. Disseminate the capacity to construct emulators to practitioners

    During this project we will organize 3 dissemination events to make our tools
    accessible to everybody.

4. Support Open Science by doing open science

    The outputs of the project will be released under [Libre Software Licenses](https://www.gnu.org/licenses/license-list.html) and [Creative Common Licenses](https://creativecommons.org/share-your-work/)


# References
[^Sedlak14]: Sedlak, D. (2015). Water 4.0: The Past, Present, and Future of the World’s Most Vital Resource. Yale University Press.

[^Reichert05]: Reichert, P., & Borsuk, M. (2005). Does high forecast uncertainty preclude effective decision support? Environmental Modelling & Software, 20(8), 991–1001. http://doi.org/10.1016/j.envsoft.2004.10.005

[^Reichert11]: Reichert, P., White, G., Bayarri, M. J., & Pitman, E. B. (2011). Mechanism-based emulation of dynamic simulation models: Concept and application in hydrology. Computational Statistics & Data Analysis, 55(4), 1638–1655. http://doi.org/10.1016/j.csda.2010.10.011

[^Carbajal16]: Carbajal, J. P., Leitão, J. P., Albert, C., & Rieckermann, J. (2017). Appraisal of data-driven and mechanistic emulators of nonlinear simulators: The case of hydrodynamic urban drainage models. Environmental Modelling & Software, 92, 17–27. http://doi.org/10.1016/j.envsoft.2017.02.006

[^Quarteroni16]: Quarteroni, A., Manzoni, A., & Negri, F. (2015). Reduced Basis Methods for Partial Differential Equations: An Introduction. Springer International Publishing.
