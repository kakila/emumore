---
layout: post
title:  "Hello world!"
date:   2017-04-21 04:00:02
author: juanpi
mathjax: false
comments: true
---

The kickoff meeting on 19th April 2017 set the project into motion!
The official starting date is 1st of May, but we are nevertheless already setting up the activities.

The name of the project is **EmuMore**, an acronym that summarizes the idea that **Emu**lation will allows us to do **More** with our models.
An emu is also a fast running bird, pure coincidence! But it gave us some ideas for a logo (it isn't finished):

![EmuMore logo]({{ "/assets/img/emu.png" | absolute_url }})

Suggestions are welcomed.

Once we got the structure of the project [source code repository](https://bitbucket.org/kakila/emumore) ready we will make it public.
Currently we are sharing just the issue tracker, which should give a nice overview of our current activities.

One of the main tasks is the preparation of Event #1, taking place on Nov. 2017.
But we are also preparing the first emulation datasets and benchmark problems.

We will keep you posted!
