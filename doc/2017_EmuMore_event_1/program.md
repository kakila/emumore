# EmuMore Event #1
This is the program of activities for the first EmuMore event.

Slides for all presentations can be downloaded from this [webpage](https://sites.google.com/site/juanpicarbajal/downloads/2017_EmuMore_event_1.zip).

## Date and location

* **When**:
  > 27th November 2017

* **Where**:
  > Room FC-D24
  >
  > Ueberlandstrasee 133, D?bendorf
  >
  > Eawag
  >
  > Switzerland

## Program

### Morning (9:00 - 12:00): host presentations and tutorials
Time slot |Title | Speaker
:----------|:-------:|:-------
09:00-09:40 | What's emulation? When do we apply emulation? How's emulation even possible? | JuanPi Carbajal
09:40-10:10 | Emulation: interpolation, "intrapolation" and extrapolation | JuanPi Carbajal
10:10-10:40 | Exploratory data analysis | JuanPi Carbajal
10:40-10:55 | **Break** |
10:55-11:25 | Regression and prior knowledge | JuanPi Carbajal
11:25-12:00 | Available resources and discussion | JuanPi Carbajal

### Lunch is not covered!

### Afternoon (14:00 - 17:00): external presentations
Time slot |Title | Speaker
:----------|:-------:|:-------
14:00-14:30 | Emulation of drainage network simulators | Mahmood Mahmoodian
14:30-15:00 | Emulation of water quality simulators | Antonio Manuel Moreno R?denas
15:00-15:10 | **Break** |
15:10-15:40 | Machine learning in geospatial modeling | Gionata Ghiggi
15:40-16:10 | Introduction to hierarchical models | Andreas Scheidegger
16:10-17:00 | Unconference |

During the unconference we will discuss your particular problem. Bring questions and ideas. It is hands-on!

### Apero (17:00-18:30)
