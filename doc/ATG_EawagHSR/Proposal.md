## Juan Pablo Carbajal - Eawag Academic Transition Grant Proposal

# Data driven modeling for aquatic research

## Summary
I am applying for funding to conclude and extend the research carried on during the EmuMore project supported by Eawag Discretionary Funds (grant no.: 5221.00492.011.05, project:  DF2017/EMUmore).
The goals are twofold: i) preparation of a joint research proposal for non-Eawag funding, ii) extend the collaboration with Dr. Blake Matthews.
By the end of the EmuMore project (estimated: 05/2019), I will start working at the HSR University of Applied Sciences Rapperswil as scientific staff in the IET Institute for Energy Technology lead by Prof. Dr Henrik Nordborg.
This position is accompanied with the possibility of starting a new group on Applied Complex Systems research, provided I can acquire external funding.
I am requesting Eawag funding (25'000 CHF, approx. 3 months PostDoc) to increase the chances of securing a leading position at HSR.

## Career benefits
The requested funding will increase the chances of submitting a successful research proposal to the Swiss National Science Foundation (SNSF).
Due to their prestige within the HSR environment, a SNSF project will further strengthen the request (to be sent on 2019/2020) for the opening of a new group on Applied Complex Systems research at HSR.


## Detailed description of activities

### Coarse grained models and qualitative differential equations (with Dr. Kris Villez)
Physical systems or processes in the real world are complex and can be understood at various levels of detail.

**Change example to chemical reaction**
For instance, a gas in a volume consists of a large number of molecules. 
But instead of modeling the motions of each particle individually (micro-level), we may choose to consider macroscopic properties of their motions such as temperature and pressure.
Our decision to use such macroscopic properties is first necessitated by practical considerations. Indeed, for all but extremely simple cases, making a measurement of all the individual molecules is practically impossible and our resources insufficient for modeling the ∼10^22 particles present per litre of ideal gas.
Furthermore, the decision for a macroscopic description level is also a pragmatic one: if we only wish to reason about temperature and pressure, a model of the individual particles is ill-suited.

Statistical physics explains how higher-level concepts such as temperature and pressure arise as statistical properties of a system of a large number of particles, justifying the
use of a macro-level model as a useful transformation of the micro-level model.
However, in many cases aggregate or indirect measurements of a complex system form the basis of a macroscopic description of the system, with little theory to explain whether this is justified or how the micro- and macro-descriptions stand in relation to each other.

Due to deliberate modeling choice or the limited ability to observe a system, differing levels of model descriptions are ubiquitous and occur, amongst possibly others, in the following three settings:

* Micro-level models versus macro-level models in which the macro-variables are aggregate features of the micro-variables; e. g. instead of modeling the brain as consisting of 100 billion neurons it can be modeled as averaged neuronal activity in distinct functional brain regions.

* Models with large numbers of variables versus models in which the ‘irrelevant’ or unobservable variables have been marginalised out; e. g. modeling blood cholesterol levels and risk of heart disease while ignoring other blood chemicals or external factors such as stress.

* Dynamical time series models versus models of their stationary behavior; e. g. modeling only the final ratios of reactants and products of a time evolving chemical reaction.

We focus on aggregated models (in some sense this is a change in the scale of the description), and in particular this strategy applied to the modeling of signals measured from SBR reactors.

#### Application
This methodology has direct application to the monitoring of SBR reactors [1], for which relevant features of measured signals are still undetermined.

### Causal analysis of pond-ecological signals (with Dr. Blake Matthews)
During the EmuMore project Dr. Carbajal analyzed data coming from the _pond experiment_ performed from June 2016 to Jan 2018.
Results indicate that abiotic properties of the ponds (e.g. heat exchange dynamics) are affected by the ecology of the pond and the nutrient treatments applied to them.
More time is needed to performs further analyses to:
 - Increase the plausibility of the results (different methods)
 - Quantify the intensity and uncertainty of the effects (detailed analysis)
If confirmed, the results are relevant for the design of further pond experiments and they evince a hitherto neglected interaction between biological populations and their environment.
Leading to an academic publication reporting the discovery.
A research project to acquire more data and extended the analyses is under consideration

## References
[1]: Schneider, M. Y., Carbajal, J. P., Furrer, V., Sterkele, B., Maurer, M., & Villez, K. (2018, December 31). Beyond signal quality: The value of unmaintained pH, dissolved oxygen, and oxidation-reduction potential sensors for remote performance monitoring of on-site sequencing batch reactors. https://doi.org/10.31224/osf.io/ndm7f
