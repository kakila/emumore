Fast surrogate models: making the impossible possible
=====================================================
```
Author: Juan Pablo Carbajal

Type  : Poster

Affil.: SWW - Eawag

Super.: Jörg Rieckermann
```

Detailed numerical simulators of complex systems play a major role in science and engineering because they allow us to make quantitative predictions about the system's behavior.
Many studies aimed at harnessing or understanding properties of these complex systems require thousands of simulations: experimental design, model calibration, uncertainty quantification, realtime control, etc.
The time needed to run a single simulation ranges from a few seconds to hours depending on the complexity of the system and the level of detail of the simulator.
The large quantity of simulations and the time needed for each run, often make these studies impossible, e.g. 2 weeks for the calibration of a sewer model with 8 unknown parameters.

One way to render these studies possible, and accomplished them in a reasonable amount of time, is to build surrogate models.
These surrogates capture only the features of the simulations that are relevant for the objective of the study.
This specialization usually make surrogates thousands of times faster than the detailed simulator, while still providing acceptable accuracy levels.

The EmuMore project was funded to apply state-of-the-art surrogation tools to simulators used at Eawag and by its partners, and to make these tools available to a wide scientific community.
Currently EmuMore is working on surrogates for simulators in: groundwater recharge (WUT), micropollutants in sewers (SWW), transports of solids in sewers (SWW), nitrification reactors (ENG), stormwater storage tank control (CH2M HILL), Wind turbine design optimization (HSR), facade pollutant leaching (HSR), and water quality (TU Delft).

The surrogation tools used in EmuMore are developed in the field of Machine Learning and due to their intrinsic mathematical nature, they might not be readably accessible to a wider scientific community.
Therefore EmuMore will provide software implementing concepts and abstractions that are grounded in the field of application.
This will be accompanied by comprehensive documentation, technical reports minimizing math/jargon, and hands-on workshops.
Current outputs of the project are available at EmuMore's webpage http://www.eawag.ch/en/department/sww/projects/using-emulators-to-do-more-things-with-our-models/

The present poster introduces the EmuMore project and the basics of surrogation.
Aligned with the project's goals, the poster also provides a decision flowchart to help researchers decide if a surrogate model will make their work more efficient.
