## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## Data points
m = @(x) (x-0.5).^2 .* exp (-(x-0.5).^2/0.5^2/2);
x = [1 1.1 1.5 1.8].'; # normalized width
y = m(x); # efficiency

pkg load gpml

likfunc  = {@likGauss};
covfunc  = {@covSEiso};
meanfunc = {@meanZero};

hyp.lik      = log(5e-8);
prior.lik{1} = 'priorClamped';

hyp.cov  = [0; 0];
hyp.mean = [];

infe         = {@infPrior, @infExact, prior};

%% Train GP
args = {meanfunc, covfunc, likfunc, x, y};
hyp           = minimize (hyp, @gp, -1e2, infe, args{:});
% Assemgle GP post structure
[nlml,~,post] = gp (hyp, infe{2}, args{:});

%%
xt        = linspace (x(1)*0.9,x(end)*1.1, 100).';
[yt dy2t] = gp (hyp, infe{2}, args{1:end-1}, post, xt);

pp = [xt yt; flipud([xt yt])];
dp = [-sqrt(dy2t); flipud(sqrt(dy2t))];
s  = [0.5 1 2 3]; 
ns = length (s);
cc = summer(2*ns);
h = plot(xt,yt,'-', x,y,'sk');#, xt,m(xt),'-k');
set (h(1), 'color', cc(1,:), 'linewidth', 2);
set(h(2),'markerfacecolor', 'k');
for i=ns:-1:1
  patch (pp(:,1),pp(:,2)+s(i)*dp, exp((cc(2*i,:)-1)/2), 'edgecolor','none');
endfor
axis tight
v = axis ();
axis([v(1:2) 0 v(4)])
xlabel ('Normalized width');
ylabel ('Efficiency');
grid on

