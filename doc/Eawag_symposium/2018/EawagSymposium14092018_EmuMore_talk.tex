\documentclass[xcolor=dvipsnames, USenglish, notes]{beamer}
% Packages for a reasonable beamer session
\usepackage[utf8]{inputenc}
\usepackage{textcomp}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage{csquotes}
\usepackage[USenglish]{babel}

\usepackage{graphicx}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{abraces}
\usepackage{mathtools}
\usepackage{bm}

\usepackage{booktabs}
\usepackage{tabularx}

\usepackage{hyperref}

\usepackage{ellipsis}

\usepackage{tikz,pgf,calc}
\usetikzlibrary{shapes.geometric, arrows, calc}
\tikzstyle{startstop} = [rectangle, rounded corners, minimum width=3cm, minimum height=1cm, text centered, text width=1.5cm, draw=black, fill=red!30]
\tikzstyle{io} = [trapezium, trapezium left angle=70, trapezium right angle=110, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=blue!30]
\tikzstyle{process} = [rectangle, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=orange!30]
\tikzstyle{decision} = [diamond, minimum width=3cm, minimum height=0.5cm, text centered, draw=black, fill=green!30]
\tikzstyle{arrow} = [thick,->,>=stealth]

\usepackage{pgfplots}
\pgfplotsset{compat=newest}

%%% References
\newlength\leftsidebar
\makeatletter
\setlength\leftsidebar{\beamer@leftsidebar}
\makeatother

\usepackage[absolute,overlay]{textpos}
\newenvironment{reference}[2]{%
  \begin{textblock*}{\textwidth}(\leftsidebar+#1,\paperheight-#2)
      \scriptsize\bgroup\color{red!50!black}}{\egroup\end{textblock*}}

%% Sources
\newcommand{\source}[1]{{\tiny source:\thinspace{\itshape {#1}}}}

\beamertemplatenavigationsymbolsempty

% Notes
\usepackage{pgfpages}
\setbeamertemplate{note page}[plain]
%\setbeameroption{show notes on second screen=bottom}
\setbeameroption{hide notes}

% Sout
\usepackage{ulem}     % gives ther \sout command

\newcommand{\calert}[1]{{\color{BrickRed}\textbf{#1}}}


\graphicspath{{../../img/}}

%----------------
% title information
\title{Fast surrogate models}
\subtitle{for science in the data age}
\author{Juan Pablo Carbajal, Jörg Rieckermann, Henrik Nordborg, Carlo Albert, Michael Burkhardt}
\institute{Eawag: Swiss Federal Institute of Aquatic Science
  and Technology\\HSR University of Applied Sciences Rapperswil}
\titlegraphic{\includegraphics[scale=1]{emu.png}\\\url{kakila.bitbucket.io/emumore/}}
\date[14.09.2018]{September 14, 2018}

% ====================================================================

\begin{document}
\maketitle

\begin{frame}{Why is EmuMore a DF?}
  \begin{itemize}
    \item Focused on supporting Eawag and partners
    \item Horizontal methodological transfer: hard to pitch it within a field
    \item Interdisciplinary
    \item Explorative: latent demand?
    \item Bonding with FH
  \end{itemize}
  \vspace{0.6em}
  
  Data science, machine learning, and complex systems researchers are experts in \textit{background fields}:

  \vspace{0.4em}
  \centering
  {\Large "Narrow expert of a vast generality"}

\end{frame}

{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}

\begin{frame}[plain]
  \begin{center}
  {\Large\textbf{A year ago ...}}
  \end{center}
\end{frame}

\begin{frame}[plain]
  \begin{center}
  \colorbox{Magenta}{\makebox[\textwidth]{\textbf{Is your simulator slow?}}}
  \includegraphics[width=0.9\textwidth]{waiting_simulation.jpg}
  \end{center}
  \textbf{for finding good parameter values, optimal design, control, \ldots}
\end{frame}

\begin{frame}[plain]
  Speed it up using \textbf{Machine Learning} algorithms\\
  \begin{center}
  \colorbox{Magenta}{\makebox[\textwidth]{\textbf{Try out emulation!}}}
  \end{center}
  \vspace{-0.5em}
  \raisebox{3.5em}{Exploit the time you were given!}
  \includegraphics[scale=1.8]{emu.png}\\
  \vfill
  Visit the \textbf{EmuMore} poster.\\

\end{frame}
}

\begin{frame}[t]{The EmuMore project}
  \framesubtitle{\url{kakila.bitbucket.io/emumore/}}
  {
  \centering
  \includegraphics[width=0.8\paperwidth]{emumore_graph2018.png}
  }
  {
  \small
  \begin{itemize}
   \item Strong Open Science alignment: reproducible, open, shared
   \item Strong Libre Software alignment: "all" code publicly available
  \end{itemize}

  \blockquote[Buckheit and Donoho]{An article about computational result is advertising, not
scholarship. The actual scholarship is the full software environment, code and data, that produced  the  result.}
  }
\end{frame}

\begin{frame}{EmuMore resources}
  \vspace{-0.5em}
  \begin{columns}[t]
  \column{0.45\paperwidth}
  {
    Case studies and collaborations
    \vspace{0.5em}
  
    \scriptsize
    \url{bitbucket.org/}\\
    {
      \setlength{\parindent}{12pt}
      \indent \url{KaKiLa/emumore}\\
      \indent \url{KaKiLa/fsludge_pub}\\
      \indent \url{KaKiLa/compoundchannel_emulator}\\
      \indent \url{KaKiLa/storagetank_emulator}\\
      \indent \url{KaKiLa/swmm_emulator}\\
      \indent \url{KaKiLa/isochlore}\\
      \indent \url{KaKiLa/flood_emulator}\\
      \indent \url{KaKiLa/gpemulation}\\
      \indent \url{dteceolica/wind_data}
    }

    \url{gitlab.com/}\\
    {
      \setlength{\parindent}{12pt}
      \indent \url{KaKiLa/sbrfeatures.git}\\
      \indent \url{KaKiLa/batchreactor-ml.git}\\
      \indent \url{KaKiLa/windturbine_HSR.git}
      \indent \url{krisvillez/Ageing_pH_sensors.git}\\
      \indent \url{aleoni/pyRCeval.git}\\
    }
  }

  \column{0.45\paperwidth}
  Master thesis outputs
  \vspace{0.5em}

  {
    \scriptsize
    \url{bitbucket.org/}\\
    {
      \setlength{\parindent}{12pt}
      \indent \url{binello7/master_thesis}\\
      \indent \url{binello7/fswof2d}\\
      \indent \url{PaTroETH/pyswmm_emulation}\\
      \indent \url{PaTroETH/data_generation}\\
      \indent \url{PaTroETH/hydrokalman}\\
    }
  }
  \vspace{0.5em}

  Curated 3rd party code
  \vspace{0.5em}
  
  {
    \scriptsize
    \url{github.com/KaKiLa/ekfukf.git}\\
    \url{bitbucket.org/KaKiLa/gpml}\\
    \url{bitbucket.org/KaKiLa/fda}\\
  }
  \vspace{0.5em}

  Talks and lectures: 
  \vspace{0.5em}

  {\scriptsize\url{gitlab.com/KaKiLa/talks.git}\\}

  \end{columns}
\end{frame}

\begin{frame}{A year of EmuMore!}
{\tiny

Kopsiaftis G., \& Carbajal, J. P. (2018+). Fast estimation of salinity isolines in saltwater intrusion. In preparation.
\vspace{3pt}

Schneider, M. Y., Carbajal, J. P., Furrer, V., Sterkele, B., Maurer, M., \& Villez, K. (2018+).
Beyond signal quality: The value of unmaintained pH, dissolved oxygen and oxidation/reduction potential sensors for remote performance monitoring of on-site SBRs. In preparation
\vspace{3pt}

Ohmura, K., Thürlimann, C. M., Kipf, M., Carbajal, J. P., \& Villez, K. (2018+). Long-term Wear and Tear of Ion-Selective pH Sensors. In preparation.
\vspace{3pt}

Englund, M., Carbajal, J. P., \& Strande, L. (2018+). Modeling of faecal sludge quantities and qualities for resource efficient and accurate design of management and treatment solutions. In preparation.
\vspace{3pt}

Strande, L., Carbajal, J. P., \& Englund, M. (2018+). Quantities and Qualities of Faecal Sludge for Planning and Management. Book chapter. In preparation.
\vspace{3pt}

Bellos, V., Carbajal, J. P., \& Leitao, J. (2018). Flood Prediction in a Compound Channel Using Machine Learning Techniques. In 5th IAHR meeting 2018. Trento, Italy. doi:10.31224/osf.io/y4ahm.
\vspace{3pt}

Rusca, S., \& Carbajal, J. P. (2018). Fast Early Flood Warning Systems Exploiting Catchment Specific Behavior. In 5th IAHR meeting 2018. Trento, Italy. doi:10.31224/osf.io/54m3x.
\vspace{3pt}

Carbajal, J. P., \& Bellos, V. (2018). An overview of the role of Machine Learning in hydraulic and hydrological modeling. In 5th IAHR meeting 2018. Trento, Italy. doi:10.31224/osf.io/wgm72
\vspace{3pt}

Guozden, T., Bianchi, E., Solarte, A., \& Carbajal, J. P. (2018). Compatibility of wind and solar energy with electricity demand in Argentina. Submitted: IEEE Trans. Sust. Ener. doi:10.31224/osf.io/s43gr
\vspace{3pt}

Scharnhorst, K. S., Carbajal, J. P., Aguilera, R. C., Sandouk, E. J., Aono, M., Stieg, A. Z., \& Gimzewski, J. K. (2018). Atomic switch networks as complex adaptive systems. Japanese Journal of Applied Physics, 57(3S2), 03ED02. doi:10.7567/JJAP.57.03ED02
\vspace{3pt}

Leitão, J. P., Carbajal, J. P., Rieckermann, J., Simões, N. E., Sá Marques, A., \& de Sousa, L. M. (2018). Identifying the best locations to install flow control devices in sewer networks to enable in-sewer storage. Journal of Hydrology, 556, 371–383. doi:10.1016/j.jhydrol.2017.11.020
\vspace{3pt}

Mahmoodian, M., Carbajal, J. P., Bellos, V., Leopold, U., Schutz, G., \& Clemens, F. (2017). Surrogate modelling for simplification of a complex urban drainage model. European Water 57:
293-297.
\vspace{3pt}

Bellos, V., Carbajal, J. P., \& Leitão, J. P. (2017). Boosting flood warning schemes with fast emulator of detailed hydrodynamic models. In AGU 2017 Fall Meeting. New Orleans: American Geophysical Union. doi:10.13140/RG.2.2.23468.97926
\vspace{3pt}

}
\end{frame}

\begin{frame}[t]{Flooding compound channel}
  \framesubtitle{\url{bitbucket.org/KaKiLa/compoundchannel_emulator}  doi:10.31224/osf.io/y4ahm}
  \begin{columns}
    \column{0.5\paperwidth}
    \centering
    \includegraphics[height=0.3\textheight]{cchannel_geo.png}
    \includegraphics[height=0.4\textheight]{cchannel_ts.png}

    \column{0.5\paperwidth}
    \begin{itemize}
      \item Topography and hydrograph
      \item $Q(t) = Q_0 + \Delta Q\left[\frac{t}{\tau}\exp(1 - \frac{t}{\tau})\right]^\beta$
      \item Predict gauge measurements as function of time
      \item Challenge: spatially distributed and coupled time series prediction
      \item Solution based on proper orthogonal decomposition
      \item About x 2'000 speed-up
    \end{itemize}
  \end{columns}

\end{frame}

\begin{frame}{Saltwater intrusion}
  \framesubtitle{\url{bitbucket.org/KaKiLa/isochlore}}
  \begin{columns}
    \column{0.5\paperwidth}
      \includegraphics[width=\textwidth]{saltwaterintrusion.png}
      \includegraphics[height=0.5\textheight]{isocloreerror.png}
    \column{0.45\paperwidth}
      \begin{itemize}
        \item Coastline, well distribution, and pumping rates
        \item Predict the shape of a 2D isoline of salt concentration
        \item Challenge: reconstruction of parametric isolines
        \item Challenge: Bifurcation in flow regime
        \item Solution based on shape descriptors (angle, width, etc.) and splines
        \item About x 3'000 speed up
      \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}[t]{Multi-stage compressors}
  \framesubtitle{\url{gitlab.com/aleoni/pyRCeval.git}}
  \begin{columns}
    \column{0.5\paperwidth}
      \centering
      \includegraphics[width=0.8\textwidth]{MANcompressor.png}\\
      \includegraphics[height=0.5\textheight]{impellercfd.png}
    \column{0.5\paperwidth}
    \begin{itemize}
      \item Thermodynamic model: $P_{\text{out}} = P_{\text{in}} \left(1 + \frac{T_{\text{out}} - T_{\text{in}}}{T_{\text{in}}}\right)^{\frac{\gamma}{\gamma -1}}$
      \item Replace impellers by an informed (mechanistic) surrogate model: dimensionally heterogeneous model
      \item Challenge: Model based realtime monitoring (soft-sensing, sensor fusion)
      \item Challenge: Realtime prediction
      \item (working) Solution: Gaussian process regression of CFD results
      \item Estimated x 5'000 speed-up
    \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}[t]{From windmills to windfarms}
  \framesubtitle{\url{bitbucket.org/dteceolica/wind_data} doi:10.31224/osf.io/s43gr\\
  \url{gitlab.com/KaKiLa/windturbine_HSR.git}}
  \centering
  \includegraphics[height=0.5\textheight]{windturbine.png}~
  \includegraphics[height=0.5\textheight]{windfarms.png}\\
  \includegraphics[width=0.8\textwidth]{windfarmPOW.png}
\end{frame}

{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}

\begin{frame}[plain]
  \centering
  \Large{\textbf{What is emulation?}}\\
  \vspace{1em}
  {\color{yellow} Emulator}: fast interpolation/approximation of a slow simulator,
  learned from simulation results at selected inputs.\\
  \vspace{2em}
  \small{Simulator: a model providing quantitative results, e.g. numerical model}
\end{frame}
}

\begin{frame}{Emulation}
  \framesubtitle{TIMTOWTDI}
  \centering
  \only<1>{\includegraphics[width=0.8\textwidth]{FanOutIn.png}}
  \only<2>{
    \includegraphics[width=0.8\textwidth]{EmulationIdea.png}
  }
  \onslide<2>{

    Internal states are lost

    Another name: Input-Output relations of complex nonlinear systems (linear case: \emph{transfer functions})
  }
\end{frame}

\begin{frame}{When is emulation useful?}
  \framesubtitle{no task, no emulator}

  Applications:
  \begin{itemize}
    \item Optimization
    \begin{itemize}
      \item System identification (a.k.a. model calibration)
      \item Preliminary and detailed engineering design (\alert{conceptual design?})
      \item Experimental design
    \end{itemize}
    \item Real-time vizualization \& feedback ((serious) games)
    \item Real-time control (if optimal then includes optimization)
    \item Digital twin (RT: data fusion, visualization, feedback, and control)
    \item \dots
  \end{itemize}
\end{frame}

\begin{frame}{Cooking recipe}
  A simple procedure for emulation:
  {
    \scriptsize
    \begin{enumerate}
      \item \label{dataset} Build a dataset by running simulations on relevant inputs
      \item \label{hypothesis} Select a model to "fit" the dataset (e.g. Gaussian Process, Artificial Neural Network, etc.).
      \item \label{split} Split your dataset into \calert{training} and \calert{test} set
      \item \label{train} "Fit" the data in the training set with your model (not an estimation of performance)
      \item \label{test} Test the model in the test set (this is an estimation of performance)
      \item Replace your simulator with the emulator, and solve the task
      \item Verify
    \end{enumerate}
  }
  Usually we iterate steps \ref{hypothesis}~--~\ref{test} (resampling), we need a third
  set for performance estimation that we never (\calert{ever}) use in the
  iteration: \calert{validation} set.

  \vspace{0.5em}
  Step \ref{dataset} might be included in the iteration: optimal design of (numerical) experiments.
\end{frame}

\begin{frame}{Machine (Human) Learning}
  \centering
  \begin{columns}[T]
    \column{.5\textwidth}
      \only<1-2>{\includegraphics[height=0.7\textheight]{horse_learning_1.png}}
      \only<3>{\includegraphics[height=0.7\textheight]{horse_learning_2.png}}
    \column{.5\textwidth}
      \begin{itemize}
        \item<1-3> What's in the picture?
        \item<2-3> Is anybody riding it?
      \end{itemize}
  \end{columns}
\end{frame}

\input{intra_extra_inter.tex}

\begin{frame}[t]{Intrapolation}
  \framesubtitle{Emulator behavior "within" the data?}

  \centering
  \only<1>{
  \vspace{-0.5em}
  \includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{intrapolation_data.pdf}
  \\Interpolation or approximation?
  }
  \only<2>{
  \vspace{-0.5em}
  \includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{intrapolation_poly.pdf}
  \\Which one is the best?
  }
\end{frame}

\begin{frame}[t]{Extrapolation}
\framesubtitle{Emulator behavior "away" from the data?}
  \vspace{-0.5em}
  \centering
  \only<1>{\includegraphics[height=0.8\textheight]{mist.jpg}
  }
  \only<2>{\includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{extrapolation_data.pdf}}
  \only<3-4>{\includegraphics[height=0.75\textheight,trim=1cm 5mm 1cm 0,clip]{extrapolation_gp.pdf}\\
  }
  \only<3>{Which one is the best?}
  \only<4>{Which one is \sout{the best} compatible with our beliefs?}
\end{frame}

\begin{frame}{Summary}
  \begin{itemize}
    \item without knowledge (assumptions) about the process, there is no "best" interpolant
    \item knowledge about the process is the "mechanistic" part
    \item knowledge about the process "shrinks" the search space
  \end{itemize}
  \vspace{1em}

  Despite the hype,  "Machine learning" is not ready to support science directly: inclusion of models is an open question.
  
  Model-free approaches cannot learn causal relations (in general)
  \vspace{0.5em}
  
  Extensive discussion: Pearl, J., \& Mackenzie, D. (2018). \textit{The Book of Why: The New Science of Cause and Effect}. Basic Books.

\end{frame}

\input{ML_and_mechanistic_modeling.tex}

\begin{frame}{DataScience@Eawag}
  \begin{itemize}
    \item DataScience@Eawag lunch seminars: Kris Villez, Stefania Russo
    \item UWE meetings: Jörg Rieckermann, Kris Villez, \ldots
    \item x3+ Principal Components Analysis workshop: Kris Villez \url{gitlab.com/KaKiLa/PCAR_toolbox.git}
    \item Control version systems workshop (git, mercurial): Harald von Waldow
    \item Collaborative open data management: Harald von Waldow
  \end{itemize}
\end{frame}

\begin{frame}{EmuMore's lessons}
  \begin{itemize}
    \item (Eawag's) latent demand of support, UK: \textit{research software engineering}
    \item Data \& software sharing (cf. linked data). Not yet there! On the way.
    \item More efficient use of resources: human, time, \& computation.
    \item Dream: complex systems departments. Motivate by usefulness.
  \end{itemize}
\end{frame}

{
\setbeamertemplate{background}{}
\setbeamercolor{background canvas}{bg=black}
\setbeamercolor{normal text}{fg=white}
\usebeamercolor[fg]{normal text}
\begin{frame}[plain]
\centering
\Huge{Thank you!}\\
\Huge{Q\&A}
\end{frame}
}

\end{document}
