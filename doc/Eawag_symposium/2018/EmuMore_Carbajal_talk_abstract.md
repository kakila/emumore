Fast surrogate models: making the impossible possible
=====================================================
```
Author: Juan Pablo Carbajal

Type  : Poster

Affil.: SWW - Eawag

Supervisor: Jörg Rieckermann (SWW, Eawag)

Collaborator: Henrik Nordborg (IET, HSR)
```

Detailed numerical simulators of complex systems play a major role in science and engineering because they allow us to make quantitative predictions about system's behavior.
Many studies aimed at harnessing or understanding properties of these complex systems require thousands of simulations: experimental design, model calibration, uncertainty quantification, real-time control, etc.
The time needed to run a single simulation ranges from a few seconds to hours depending on the complexity of the system and the level of detail of the simulator.
The large quantity of simulations and the time needed for each run, often make these studies impossible, e.g. 2 weeks for the calibration of a sewer model with 8 unknown parameters.

One way to render these studies possible, and accomplished them in a reasonable amount of time, is to build surrogate models.
These surrogates capture only the features of the simulations that are relevant for the objective of the study.
This specialization usually make surrogates thousands of times faster than the detailed simulator, while still providing acceptable accuracy levels.

The EmuMore project was funded to apply state-of-the-art surrogation tools to simulators used at Eawag and by its partners, and to make these tools available to a wide scientific community.
EmuMore has provided effective emulators of simulators in: stormwater storage tank control (CH2M HILL), riverine hydrology (CH2M HILL), flood alarm systems (SWW), water quality (TU Delft),
multi-stage turbo compressors (HSR), and wind farms power output (UNRN, Argetina).
We are still actively collaborating with several other partners.

During the execution of the project we provided consult and support in mathematical modelling  and software development, for several partners that were not ready for surrogation.
We also completed two successful ETH Master theses.
these students will help spreading the methods among practitioners in the field of urban hydrology.
Current outputs of the project are available at EmuMore's webpage [https://kakila.bitbucket.io/emumore/](https://kakila.bitbucket.io/emumore/)

The present talk introduces the EmuMore project and the basics of surrogation.
Several works on emulation and modeling carried on during 2018 are showcased.
