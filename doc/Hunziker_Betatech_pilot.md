> Copyright (C) 2017 - Juan Pablo Carbajal
>
> This work is licensed under a
> Creative Commons Attribution-ShareAlike 4.0 International License.
>
> You should have received a copy of the license along with this
> work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

# Emulation for WWTP realtime operation

To update operational parameters (OP) on a dynamic way, models are needed to estimate the effect that changes on the OP have on the performance of the WWTP.

Available models are to slow to implement a dynamic update of OPs, specially when uncertainties are to be quantified or when the OP update should be optimal in some predefined sense (e.g. optimal control).

To avoid large investment on IT infrastructure (HPC cluster/services, data transfer, etc...), we consider emulation to be an alternative to evaluate.
After successful emulation, out-of-the-shelf personal computers can be used for model predictions and optimizations.

Emulation will be performed by collaborators at Eawag in the context of the [EmuMore project](https://kakila.bitbucket.io/emumore/).

Know-how will be transferred from Eawag to the partners in the project.

In the pilot-project no intellectual property claims will be made by the private partners.

## Models
The models considered for emulation are:

1. The inflow to the WWTP (water quantity)

2. The quality of the inflow to the WWTP (water quality)

3. The performance of a batch reactor

4. The function to calculate operational parameters, e.g. set-points, reactor schedule, etc.

In the pilot-project we will develop an emulator of the 1st model.
The Eawag collaborators have ample experience with this kind of emulators.

The emulation method strongly depends on the definition of the predictive task, hence no commitment to a method can be specified at this point.
The list of methods that can be used for emulation is vast: Random forests, Gaussian Processes, (classical/extended/unscented) Kalman filters, or Polynomial Chaos Expansion, to mention just a few.

## Milestones

1. Predictive task is fully defined: input ranges and relevant outputs, emulation quality. At his stage the list of relevant emulation methods can be defined.

2. 1st dataset is built: simulator is executed for a sample of the input space.

3. 1st emulator is built and evaluated.

4. Refined dataset is built: if the 1st dataset is not enough to achieve desired emulation quality, it is extended.

5. Refined/final emulator is built and evaluated


## Data sharing
Eawag needs to have unrestricted access to dataset produced by the simulators (not to the simulators themselves). 
For example, Eawag might use the datasets for other activities beyond this project (e.g.g publications, teaching, other projects), honoring ethical scientific behavior.

A recommendation is to share the datasets under the [Creative Commons Attribution-ShareAlike 4.0 International License](<http://creativecommons.org/licenses/by-sa/4.0/>).

## Funds for pilot project

No direct funds for Eawag are requested for the pilot project, since the time of Eawag collaborators is already funded by the [EmuMore project](https://kakila.bitbucket.io/emumore/).

There might be costs on the Hunziker-Betatech side, due to the time needed to generate and share the data.



