## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>


dot realnet1.gv -Tsvg > realnet1.svg
dot realnet2.gv -Tsvg > realnet2.svg
dot realnet3.gv -Tsvg > realnet3.svg
dot canonical_real.gv -Tsvg > canonical_real.svg

dot swmmnet1.gv -Tsvg > swmmnet1.svg
dot swmmnet2.gv -Tsvg > swmmnet2.svg
dot swmmnet3.gv -Tsvg > swmmnet3.svg
dot canonical_swmm1.gv -Tsvg > canonical_swmm1.svg
dot canonical_swmm2.gv -Tsvg > canonical_swmm2.svg
dot canonical_swmm3.gv -Tsvg > canonical_swmm3.svg
