Kickoff meeting DoMor
=====================
WED 19.04., Eawag, FC-F77

Participants
------------

* Joerg Rieckermann, joerg.rieckermann@eawag.ch
* Juan Pablo Carbajal, juanpablo.carbajal@eawag.ch
* Carlo Albert, carlo.albert@eawag.ch
* Jonas Sukys, jonas.sukys@eawag.ch
* (Henrik Nordborg)
* (Michael Burkhardt)

Goals
------

**Fill-in project** [Charter](https://docs.google.com/document/d/1VuwmUwonPqgDVLnvbxTmJ8Qpp7Gb8Bc4J2FodLIsX0c/edit)

* Re-visit goals and schedule (GANTT-chart)
* Agree on scientific output
* Agree on applied products
* Adjust schedule to output
* Define general aspects of budget, project organisation, stakeholders, etc.
* Define next steps



Time | Topic| Who
-----|------|-----
14:00-14:20| Intro+ re-visit proposal: *Goals, *Products, *Schedule, *Budget |JR
14:20-14:45| Current situation+boundary conditions and own goals |JPC
14:45-15:45| Detailed discussion: 1.+2. (scientific) output, 3. events, 4. deliverables, 5. schedule, 6. collaborators, 7. etc.  |all
15:45-16:00| break | all
16:00-16:45| Fill in Project charter | all
16:45-17:00| Wrap-up, conclusions | JR


1. Re-think the balance between scientific aspirations and case studies and education.

    A: If we are successful with many case studies, researchers @Eawag and practice-partners would
     possibly generate a steady revenue-stream for HSR, which would make it possible to sustain
     continued developments.

    A': If we do not do the basic science now, while we do not have worry
     too much about results, we'll never be able to do it later.

2. suggested Products:
      * Articles:
          * _Emulation from the renomalization perspective_
          * _Input compression for effective (hydrological) emulation_
          * _Data-driven reduced basis methods for parametrized PDE models_
          * _Online and real-time PDE model calibration using emulators with applications to nonlinear model predictive control_
          * _Fast and exhaustive model calibration using emulators of XXX model_
emulate.
     * Sotfware library
     * Workshops => case studies
     * Tech reports => which ones? Tentative titles

3. Dissemination events: the project proposed 3 events, one of them at
HSR. We need to define contents, type of event and start the
organization of the first (originally between month 6 and 10)

4. Internal deadlines and deliverables: this is a one 80% man project,
so it runs in series, hence I want to frame your expectations
correctly.

5. "Gantt" chart of the project: There is a tentative chart in the
proposal (sent by Joerg on 20.02 subject: DF 2017), but we need to
adjust it a little bit base don expectations of the group.

6. Think about collaborators: Sudret from ETH is an obvious one. SDSC might support us with infrastructure, or special know-how...

7. Organization of Master students: from Eawag? from HSR? when?

8. COMLEAM collaboration at HSR: uncertainty quantification / upscaling of wheather driven organics leaching processes.

9. Working place and times: schedule.

10. budget

[Agenda in layman's GDoc](https://docs.google.com/document/d/1YnNCZYaW5ngryAxNIH2BMoAC94gIRrAZy8F8-SHazGI/edit#)

Micro-tasks
-----------

* Find out next relevant conferences (e.g. EPFL Quarteroni, ETA: 1 day)

* Contact beneficiaries and invite them to 1st Event (ETA 1 week + undefined)
    * Event scope and content
    * Event format
    * Event date
    * Send invitations
    * Begin model+dataset collection (REACON 1+0)

* Emulate Fehraltdorf SWMM model (ETA: 8 days)
    * Model file
    * Define problem
    * Build dataset

* Emulation of nitrification reactor (ETA: 8 days + 2 months)
    * Get numerical model
    * Define problem
    * Build dataset
    * Emulate & compare with Shape-constrained
    * Get experimental data
    * Enhance emulator with data

* Setup reporting infrastructure (ETA: 1 day)
    * Datbase sharing system (e.g. Zenodo)
    * Project blog (e.g. Wordpress)
    * Repostiory + Wiki (done: bitbucket)

* PDE benchmark problems (Libre software) (ETA: 3 months)

    For each problem we try to set up all combinations of {linear, non-linear, stationry, time-dependent, control}

    * Define tool(s): FeNics, OpenFoam, Custom, ...
    * Heat equation
    * Reaction-Convection-Diffusion equation
    * Fluid dynamics (NS, StV, etc...?)

* ODE bechmark problems (ETA: 1 month)

   By fixing the space discretization all previous benchmarks
   can be used here, some extra intrinsic ODE problems:

   * 1st order
       * Connected tanks
       * logSPM
   * 2nd order
       * NL springs
   * Mixed 1st and 2nd order systems
   * Algebraic

* Implement ROM algorithms (ETA: 2 months)
    * Projection methods
    * Prior and posterior error estimation
    * Empirical interpolation methods (EIM)
    * Enhanced EIM with GP

* Implement GP to Kalman (ETA: 1 month)
    * Collaboration with N. Hanes (GPML toolbox)
