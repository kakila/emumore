Kickoff meeting DoMor
=====================
WED 19.04.2017, Eawag, FC-F77

Participants
------------

* Joerg Rieckermann (JR) <joerg.rieckermann@eawag.ch>
* Juan Pablo Carbajal (JP) <juanpablo.carbajal@eawag.ch>
* Carlo Albert (CA) <carlo.albert@eawag.ch>
* Jonas Sukys (JS) <jonas.sukys@eawag.ch>

## Table of contents

[TOC]

## Intro and re-visit proposal
We went through the submitted proposal and
explained the original idea.

We discussed the reasons for the dissemination events and their function within the project.

* Event 1 (internal, 4th week, November 2017)
    1. Identify interested beneficiaries (there is a table in the proposal) and collect their problems and datasets.
    2. Intorduce them to emulation methods
    3. Help them better define their problems

* Event 2 (public, TBA, somewhen in 2018)
    1. Showcase the methods developed up to this point of the project.
    2. Showcase the results obtained with the participants in Event 1.

* Event 3 (public, TBA, somewhen in 2019 at HSR)
    1. Showcase prodcuts of the project to industry and other institutions

We answered all questions and doubts expressed by particiapnts other than JR and JPC

## Current situation and goals
JPC described the current siutation of the project and the expected outputs.

JPC provided a list of specifc task that cover his time for roughly the first ~6 months.

### Publications
* Methodological:
     1. _Input compression for effective (hydrological) emulation_
     2. _Data-driven reduced basis methods for parametrized PDE models_

    In the papers above we will be leading authors.

* Applied:
    1. _Online and real-time PDE model calibration using emulators with applications to nonlinear model predictive control_
    2. _Fast and exhaustive model calibration using emulators of XXX model_

    In 1. we will be leading authors.

    We expect that papers of the 2. class we are just contirbutors

* Fancy extras:
     * _Emulation from the renomalization perspective_

### Specific tasks

* **Emulation with currently available methods**
    * Emulate Fehraltdorf SWMM model (ETA: 8 days)
        * Model file
        * Define problem
        * Build dataset
    
    * Emulation of nitrification reactor (ETA: 8 days + 2 months)
        * Get numerical model
        * Define problem
        * Build dataset
        * Emulate & compare with Shape-constrained
        * Get experimental data
        * Enhance emulator with data
* **Coding**
    * Compile benchmark problems
    
    * Implement ROM algorithms (ETA: 2 months)
        * Projection methods
        * Prior and posterior error estimation
        * Empirical interpolation methods (EIM)
        * Enhanced EIM with GP

* * **Admin and reporting**
    * Contact beneficiaries and invite them to 1st Event (ETA 1 week + undefined)
        * Event scope and content
        * Event format
        * Event date
        * Send invitations
        * Begin model+dataset collection (REACON 1+0)
    
    * Setup reporting infrastructure (ETA: 1 day)
        * Datbase sharing system (e.g. Zenodo)
        * Project blog (e.g. Wordpress)
        * Repostiory + Wiki (done: bitbucket)

    * Find out next relevant conferences (e.g. EPFL Quarteroni, ETA: 1 day)

* **Extras**
    * Implement GP to Kalman (ETA: 1 month)
        * Collaboration with N. Hanes (GPML toolbox)
    

## Meetings
The whole group will meet every 3 months

We agreed to reserve a 2 hr. slot every month, we will not use them always.
It is just to have a reserved slot to meet.
The slot is on a Monday.
More details TBA

## Reporting
Periodic public blog posts will be publish summarizing recent developments. One-to-many reporting.

The Wiki of the repostory will contain the details. Many-to-Many reporting

Approximatelly every 6 months (event driven) we will compile and publish a detialed progress/technical report that should be used as reference.

## Synergies with other projects and new beneficiaries
JS expressed his interest in the project and stated that he could contribute a high-level tool to manage database creation for slow simulators.
This tool can be essential for many Eawag people using simulators.
This is aligned with his tasks in other projects.

CA metioned a simulation project on Selenium depositon that could benefit from emulation. He will provide contact details.

CA is also working in input compression in the "Signature ..." project. We should coordinate to prevent too much overlapping.









