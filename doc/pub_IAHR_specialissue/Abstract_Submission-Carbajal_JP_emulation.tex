\documentclass[12pt,a4paper,english,twocolumn,fleqn]{narms}

% packages needed
\usepackage{subfigure}
\usepackage{epsfig}
\usepackage{timesmt}

% add here more packages based on the document format
\usepackage[usenames,dvipsnames,svgnames]{xcolor}
\usepackage[numbers, sort&compress]{natbib}

\usepackage{babel}
\usepackage{graphicx}
\usepackage{bm}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\newcommand{\braket}[2]{\langle {#1} , {#2} \rangle}
\newcommand{\ud}{\mathrm{d}}
\newcommand{\inter}[2]{\int {#1}\ud {#2}}

% Operators
\DeclareMathOperator*{\armin}{arg\,min}

\usepackage{siunitx}
%\sisetup{mode=text,range-phrase = {\text{~to~}}}
\sisetup{
  range-phrase = {,}\ ,
  range-units  = brackets,
  open-bracket = [,
  close-bracket= ],
}

\usepackage[colorlinks=true]{hyperref} % Hyperlinks within and outside the document
% false: boxed links; true: colored links

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage{lmodern} % German related symbols

\usepackage{tikz,pgf,calc}
\usetikzlibrary{shapes.geometric, arrows, calc}
\tikzstyle{startstop} = [rectangle, rounded corners, minimum width=3cm, minimum height=1cm, text centered, text width=1.5cm, draw=black, fill=red!30]
\tikzstyle{io} = [trapezium, trapezium left angle=70, trapezium right angle=110, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=blue!30]
\tikzstyle{process} = [rectangle, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=orange!30]
\tikzstyle{decision} = [diamond, minimum width=3cm, minimum height=0.5cm, text centered, draw=black, fill=green!30]
\tikzstyle{arrow} = [thick,->,>=stealth]

% setting math equation indent from left 0pts
\mathindent=0pt%

%%%%%%% Style for TABLES
% insert tabular command inside \tabletext{} this will produce tables in 10pts

\defcitealias{Octave}{GNU Octave}
\defcitealias{Inkscape}{Inkscape}

\newcommand{\jpi}[1]{{\color{Magenta} JPi: #1}}
\newcommand{\seb}[1]{{\color{BrickRed} Vsl: #1}}

\begin{document}
\title{}
\author{{J. P. Carbajal} \\
{\aff{Swiss Federal Institute of Aquatic Science and Technology, Eawag, Dübendorf, Switzerland}} \\\\
{\authornext{V. Bellos}}\\
{\aff{National Technical University of Athens, Greece}}
} \maketitle


\section*{Abstract}
  The use of Machine Learning (ML) in hydrology and hydraulic is framed within a wider scientific context.
  The aim is to provide potentially illuminating interdisciplinary perspectives, and minimize the creation of endogenous jargon.

\section{Overview: Machine learning \& general problem statement}

ML methods are pervasive in all quantitative scientific fields.
Some of these are assimilated so deeply by the local community that have been re-baptized, and its original connection with ML almost lost.
When this happens there is a risk of re-working results already present in ML (or other fields using the same methods).
That is, the risk of spending time in solved issues or missing contributions from other fields to the given problematic.

There is great value in rediscovery, the problem is to actively avoid realizing it.
Most often than not independent rediscovery leads to knowledge unification and defragmentation.
%Being aware that our aptly-named-purposed-to-be-novel method is related with (or equivalent to) some other method adds value to our contribution.
Equivalent and complementary views on a problem have historically lead to simplifications and deeper abstractions.
However, these breakthroughs are often lost in impermeable noöspheres and jargon bubbles, which payoff in the current academic evaluation system.
%To pierce these bubbles the would-be-creator of a method needs to make themselves aware of developments in other fields that might be linked to their work, i.e. do re-search.
%Regrettably, the sizable intellectual effort needed to pierce these bubbles is seldom rewarded in academia\footnote{Applied communities contributing to fundamental research like ML, optimization, AI, etc. are also responsible, and should be rewarded, for exercising outreach and reduce the hurdles for professionals in other fields to access their results: this mainly entails avoiding the jargon}.
%Then need is hence clear: we need to reinforce this sort of old-school research.
%Ironically, doing research with today technologies is easier than ever was, but the same technologies also ease the creation of noise, i.e. inconsequential results, obfuscating rewordings, etc.; leading to loads of publications (again paying off in the current academic system).
The need to pierce these bubbles is hence clear.
Ironically, doing research with today technologies is easier than ever was, but the same technologies also ease the creation of noise, i.e. inconsequential results, obfuscating rewordings, etc.; leading to loads of publications.

In an attempt to contain the spread of this plague of the age into hydrology \& hydraulic research (hydroresearch) using ML, we attempt to raise awareness about the issue and provide some tools to allow the hydro-researcher to frame their problems in a wider context.
In particular we focus herein in the use of ML to build fast approximate replacements for detailed simulators of real-world phenomena, henceforth called \emph{emulators}.

\section{The learning problem}
A description of ML could be "to use a set of observations to uncover an underlying process", where the underlying process is a functional pattern, herein called \emph{unknown target function} (see Fig.~\ref{fig:learning} and~\citep{AbuMostafa12} for details).
The available data, i.e. the \emph{training examples}, are samples from the underlying process and they represent all the direct information we have about it.

To search for the target function, researchers choose an extensive set of functions.
This \emph{hypothesis set}, is believed to contain the unknown target function or at least a very good approximation for it.
This choice is based on their previous experience and the available expert knowledge about the process.

The \emph{learning algorithm} uses the training data to select from the hypothesis set the best candidate function, i.e. the \emph{final hypothesis}.
Since all prior information about the unknown target function is encoded in the hypothesis set, the quality of the best candidate heavily depends on the elements in the set.

\begin{figure}[htpb]
  \centering
    \begin{tikzpicture}[auto,node distance=3em]
     \tikzset{rect/.style={rectangle,
                           text centered,
                           fill=GreenYellow!40,
                           draw}}
     \tikzset{rrect/.style={rectangle,
                            rounded corners,
                            thick,
                            draw=BrickRed}}

     \node[rrect, text width=4em] (ML)
     {
       \textbf{\small{Learning Algorithm}}
     };
     \node[rect] (trn)[above of=ML, xshift=-1.5cm]
     {
       \textbf{\small{Training Examples}}
     };
      \node[rect] (func)[above of=trn]
      {
        \textbf{\small{Unknown Target Function}}
      };
      \node[rect] (prior)[below of=ML, xshift=-1.5cm, yshift=-0.5em]
      {
       \textbf{\small{Hypothesis Set}}
      };
      \node[rect] (post)[right of=ML, xshift=2cm]
      {
        \textbf{\small{Final Hypothesis}}
      };

    \path[thick,->,>=stealth', shorten <=2pt, shorten >=2pt]
      (func)  edge  (trn)
      (trn)   edge[bend right]  (ML.west)
      (prior) edge[bend left]  (ML.west)
      (ML)    edge  (post);
    \end{tikzpicture}
  \caption{\label{fig:learning} Learning from data. }
\end{figure}

%\subsection{The hypothesis set}
%A very common and general way of describing the hypothesis set is present in by the expression
%\begin{multline}
%\textit{\small Objective} = \textit{\small Distance to training examples } + \\
%\textit{\small "Complexity" of the final hypothesis}
%\label{eq:problem}
%\end{multline}
%\noindent The learning algorithm's task is to minimize the $\textit{Objective}$, i.e. to minimize the distance between the final hypothesis and the training example and, at the same time, to keep the solution "simple". 
%The use of the words "simple" and "complex" in this context hints towards regularization,
%but the $\textit{"Complexity"}$ term in the equation can be used to select a subset of the hypothesis set with desired properties.

\section{Applications in Hydrology \& Hydraulics}
Most simulators in hydroresearch are based on deterministic differential equations (DE) describing the behavior of fluids and their interaction with the environment.
When performing emulation the target function is the response of the simulator to all physically meaningful inputs.
That is the hypothesis set contains all possible simulator outputs (admissible solutions to the DE), and the best available knowledge we can provide to our emulator is the DE itself.
The training examples are a dataset of simulations corresponding to a representative set of inputs.

%Depending on the situation we might have even more information available for our emulator: symmetries, conservation laws, etc.
%How to effectively include all types of prior information into emulator construction is a fruitful and still open research field.

In this scenario the learning algorithm can be implemented as the search of a function in the hypothesis set that solves an optimization problem, e.g.:

\begin{equation}
f = \armin_{g \in \mathcal{H}} \lambda_1 \sum \left(y_i - g(x_i)\right)^2 + \lambda_2 ||\mathcal{D}_\theta g(x)||^2\label{eq:opt_problem}
\end{equation}

\noindent where $\mathcal{D}_\theta$ is the differential operator with parameters $\theta$.
Eq.~\eqref{eq:opt_problem} states that the function $f$ approximates the data $(x_i,y_i)$ and it is "almost" a response of our simulator (e.g. Shallow water equations).
%\begin{equation}
%\mathcal{D}_\theta g(x) = 0
%\end{equation}
%The first term in~\eqref{eq:opt_problem} measures the distance between the data and the sought function at observed points $f(x_i)$ is measured in the Euclidean sense, and $f$ is close to the nullity of the differential operator.
There are several ways of looking at this problem: i) the first term is a constraint (is required to be exactly zero), ii) the second term is a constraint, iii) none of the terms is a constraint, or iv) both terms are constraints ($f$ realizes a zero of the cost function). 
Case i) corresponds to interpolation and is very relevant in emulation.
Case ii) searches for a response of our simulator $f$ that approximates the data~\citep{Poggio1990}.
Case iii) is a regularized regression problem~\citep{Ramsay2017, azzimonti12}.
And finally, case iv) gives a solution that interpolates the data and is a repsonse of our simulator (this might not be always possible)~\citep{}

%, while the first one is useful when the simulator is stochastic or we are fusing experimental data with our emulator.

%\subsubsection{Forward (simulation) and inverse problems}
%Fwd or Inv is relative respect to expert knowledge

%- abstraction level of the pior information (or believes): at which level are they available, inverse for one might be direct for others!
%mechanistic or observational? (Fabio comment about observation operator)
%In this case the regularization term takes a particular form, 

%\subsection{Mindest: type and amount of data, objective of the study}
%We need to be aware of (or make explicit) the "mindset" (the assumptions, many times informal)
%from wich we tackle the issues:

%- Infinite data? (Shawb)

%- Model discrepancies (the ones we know and the ones we know we do not know, Goldstein)

%- Exploit prior knowledge or not (data driven or mechanistic): strict or penalization?

%\subsection{The value of comparisons}
%Mathematical objects vs. their implementation

%- Types of statements we can do: Mathematical/formal vs. implementation (XX perfomrs better than YY or YY cannot do it. Dudeney).

%Classes of equivalence: as formal objects as numerical objects, as implemented numerical objects (hardware: e.g. pointers in mechanical calulators, IBM efficient computation, analog computation, Embedded AI).

\section{Perspectives}
We are not promoters of notational imperialism and suggest a unification of language across all fields, but rather we ask for scientist to be conscious of the issue and actively avoid it, and to help linking apparently disconnected fields.

- the mindset: sparse data, infinite data?

- Model discrepancies

- Exploit prior knowledge or not (data driven or mechanistic): strict or penalization?

%\subsection{Model reduction vs. emulation}
%\subsubsection{renormalization (coarse graining)}

%Statistical mechanics, up/downscaling

%Wolpert's artificial science

%\subsection{Libre software, Open datasets, and benchmarks}
%\subsection{Jargon and interdisciplinarity}
%How to make transfer of knowledge systematic (instead of circumstantial) without faling in a sort of methodological and language imperialism?

%- The jargon issue (discourse tools: the old orange book). Compression/encryption is efficent only if you know how to decompress.

%- There is a payoff in creating bubbles in terms of sucess in the current academic system

%- There is much to loose for science: wastefull use of resources, confusion, unsustainable growth, slower development

%- What programming language do you use? Why it is irrelevant from the scientific perspective and very relevant from the implementation (being efficient) persepctive.

%- Expresiveness (the easiness of representing our ideas: Feymann reinvetion of sine and coside: do it myself first) of a language is subjective: how I build my abstractions? what language allows me to do that in a natural way (this is the sibjective part), CAS peple are very aware "what's the simplents form of an expression? it is subjective, can be concesnsuated (but it is risky!)".

% Uniform language can hinder expresiveness and reinforce bubbles (resistance cells): One persons's rigor is another persons mortis. 
% 
%- Value in diferentiation and specification: cells, stem-cells. Abstraction == unification but not always good (good for understadnig, sometimes good for implementations).

%How can I handle the multiple names of things? How can this summary can be consistent with its message?

%- Geometric progression --> the agent provides n links. A search algorithm can be constructed. We need to develop tools to fcilitate thi, citation is a partial solution, keywords is useless (reinforce jargon). Scaffolding

\section{Acknowledgements}
The authors would like to thank Dr. Jörg Rieckermann for his continuous support.
The research leading to these results has received funding from the Eawag's discretionary funding program (project EmuMore \url{kakila.bitbucket.io/emumore}).

\sloppy
\bibliographystyle{unsrtnat}
\bibliography{References}

\end{document}
