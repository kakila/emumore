\documentclass[12pt,a4paper,english,fleqn]{article}

% packages needed
\usepackage{caption}
\usepackage{subcaption}
\usepackage{epsfig}
\usepackage{timesmt}

% add here more packages based on the document format
\usepackage[usenames,dvipsnames,svgnames]{xcolor}
\usepackage[numbers, sort&compress]{natbib}

\usepackage{babel}
\usepackage{graphicx}
\usepackage{bm}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
% Collection of useful mathematical symbols and commands
% Calculus
\newcommand{\ud}{\mathrm{d}}
\newcommand{\pder}[2]{\frac{\partial{#1}}{\partial{#2}}}
\newcommand{\dpder}[2]{\frac{\partial^2{#1}}{\partial{#2^2}}}
\newcommand{\sderp}[3]{\frac{\partial^2{#1}}{\partial{#2}\partial{#3}}}
\newcommand{\tder}[2]{\frac{\ud{#1}}{\ud{#2}}}
\newcommand{\rot}[1]{\nabla \times {#1}}
\newcommand{\diver}[1]{\nabla \cdot {#1}}
\newcommand{\definter}[4]{\int_{#1}^{#2} {#3}\ud {#4}}
\newcommand{\inter}[2]{\int {#1}\ud {#2}}
\newcommand{\braket}[2]{\langle {#1} , {#2} \rangle}
% Misc
\newcommand{\eval}[1]{\Big |_{#1}}
\newcommand{\bset}[1]{\big\lbrace {#1} \big\rbrace}
\newcommand{\stimes}[2]{{{#1}\!\times\!{#2}}}
\newcommand{\trp}{\top}
\newcommand{\preup}[2]{{}^{#2}\!{#1}}

% Operators
\DeclareMathOperator*{\armin}{arg\,min}
\DeclareMathOperator*{\armax}{arg\,max}
\DeclareMathOperator*{\rank}{rank}
\DeclareMathOperator*{\cov}{cov}
\DeclareMathOperator*{\nullsp}{null}
\DeclareMathOperator*{\Loss}{Loss} % Loss function

% Logicals
\newcommand{\suchthat}{\big \backslash \;}

\usepackage{siunitx}
%\sisetup{mode=text,range-phrase = {\text{~to~}}}
\sisetup{
  range-phrase = {,}\ ,
  range-units  = brackets,
  open-bracket = [,
  close-bracket= ],
}

\usepackage[colorlinks=true]{hyperref} % Hyperlinks within and outside the document
% false: boxed links; true: colored links

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage{lmodern} % German related symbols

\usepackage{tikz,pgf,calc}
\usetikzlibrary{shapes.geometric, arrows, calc, patterns}
\tikzstyle{startstop} = [rectangle, rounded corners, minimum width=3cm, minimum height=1cm, text centered, text width=1.5cm, draw=black, fill=red!30]
\tikzstyle{io} = [trapezium, trapezium left angle=70, trapezium right angle=110, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=blue!30]
\tikzstyle{process} = [rectangle, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=orange!30]
\tikzstyle{decision} = [diamond, minimum width=3cm, minimum height=0.5cm, text centered, draw=black, fill=green!30]
\tikzstyle{arrow} = [thick,->,>=stealth]

% setting math equation indent from left 0pts
\mathindent=0pt%

\newcommand{\jpi}[1]{{\color{Magenta} JPi: #1}}
\newcommand{\seb}[1]{{\color{BrickRed} Vsl: #1}}

\begin{document}
\title{An overview of the role of Machine Learning in hydraulic and hydrological modeling}
%\author{{J. P. Carbajal} \\
%{\aff{Swiss Federal Institute of Aquatic Science and Technology, Eawag, Dübendorf, Switzerland}} \\
%{\authornext{V. Bellos}}\\
%{\aff{National Technical University of Athens, Greece}}
%} 
\maketitle

\section*{Abstract}
We provide an overview of Machine learning (ML) and its role in hydrology and hydraulic.
The aim is to ease the access of researchers in the latter fields to the techniques in ML.

\section{Introduction}
Machine Learning (ML) has received increasing attention in the later years.
It promises to ease the problem of modeling from observations.
It is a heavily mathematized field, with strong statistical jargon.
Therefore, it can be difficult to access for researchers from the fields of hydrology and hydraulics (hydro-research).
A bird's-eye view of ML and its role, can be beneficial for the hydro-research community.
Herein we address the question of what is ML in general terms and what is its role in hydro-research.
We discuss some important features in each of these roles.

\paragraph{Machine learning at a glance}
ML could be described as "the use of a set of observations to uncover an underlying process".
The process is usually stated as a mathematical relation between the observations.
Herein we consider only the case in which observations include the inputs and outputs of the process, i.e. supervised learning.
The sought functional pattern, called \emph{unknown target function} establishes the relation between the inputs and the outputs, i.e. is a model of the process.
The \emph{training examples}, are input-output samples from the underlying process and they represent all the direct information we have about it.
Fig.~\ref{fig:learning} illustrates the structure of ML in this case~\citep{AbuMostafa12}.

\begin{figure}[htpb]
  \centering
    \begin{tikzpicture}[auto,node distance=3em]
     \tikzset{rect/.style={rectangle,
                           text centered,
                           fill=GreenYellow!40,
                           draw}}
     \tikzset{rrect/.style={rectangle,
                            rounded corners,
                            thick,
                            draw=BrickRed}}

     \node[rrect, text width=4.5em] (ML)
     {
       \textbf{\small{Learning Algorithm}}
     };
     \node[rect] (trn)[above of=ML, xshift=-1.5cm]
     {
       \textbf{\small{Training Examples}}
     };
      \node[rect] (func)[above of=trn]
      {
        \textbf{\small{Unknown Target Function}}
      };
      \node[rect] (prior)[below of=ML, xshift=-1.5cm, yshift=-0.5em]
      {
       \textbf{\small{Hypothesis Set}}
      };
      \node[rect] (post)[right of=ML, xshift=2cm]
      {
        \textbf{\small{Final Hypothesis}}
      };

    \path[thick,->,>=stealth', shorten <=2pt, shorten >=2pt]
      (func)  edge  (trn)
      (trn)   edge[bend right]  (ML.west)
      (prior) edge[bend left]  (ML.west)
      (ML)    edge  (post);
    \end{tikzpicture}
  \caption{\label{fig:learning} Bird's eye view of ML. Taken from "Learning from data" by Yaser S. Abu-Mostafa et al.}
\end{figure}

To search for the target function we choose an extensive set of functions, which we call the \emph{hypothesis set}.
For example, we could choose all the linear functions between inputs and outputs (i.e. linear models), or all the functions generated by a given neural network.
The key point is that the hypothesis set is built so as to contain the unknown target function or at least a very good approximation for it.
This choice is based on our previous experience and the available expert knowledge about the underlying process, e.g. mathematical or phenomenological models.
The \emph{learning algorithm} uses the training examples to select the best candidate function from the hypothesis set, i.e. the \emph{final hypothesis}.
Since all prior information about the unknown target function is encoded in the hypothesis set, the quality of the best candidate heavily depends on the elements in the set.

All fundamental research in supervised ML consist in developing new learning algorithms (mainly optimization algorithms), novel or concise descriptions of different hypothesis sets, and useful representations for input-output data (encodings).

\vspace{-0.2em}
\section{ML in hydro-research}

ML as described before can be useful for hydro-research in at least three situations: i) (artificial science) learning new models from measured data; ii) (scientific numerical modeling) using data to find the value of parameters of known models; iii) (emulation) replacing a model with a simpler version while maintaining the quality of the predictions.

The relation between these situations is summarized in Fig~\ref{fig:mlhydro}.

\begin{figure}[htpb]
  \centering
    \begin{tikzpicture}
      \draw (0,0) circle (2cm) [fill=blue!50] node [above left, text width=4em, xshift=0.3cm]{Artificial science (ML)};
      \draw (-60:1.0cm) circle (1.0cm) [fill=green!50] node {Emulation};
      \draw (0:2.5cm) circle (2cm) [fill=red, fill opacity=0.5] node [right, text width=5em, fill opacity=1] {Scientific numerical modeling};
    \end{tikzpicture}
  \caption{\label{fig:mlhydro} Three uses of ML in hydro-research. Emulation is a subset of pure ML (artificial science), and it intersect with numerical modeling when prior knowledge is exploited.}
\end{figure}

\paragraph{Artificial science}
This is the realm of pure ML in which a relation is learned using only data.
We use ML to discover new natural laws based only on experimental data.
Many popular ML techniques, specially artificial neural networks, have been seen
as the grail to perform artificial science, but none have been up to the task.
There is a long standing discussion about the topic but the main issue is that these 
tools fail to create abstractions the way human researchers do.~\citep{Marcus2018,Somers2017}
The quest is a valid scientific endeavor, but the question itself still eludes a scientific formulation and scientist should be aware that we are far from obtaining such technology.
Nevertheless, it was very popular in hydro-research during the last two decades.
Besides the mentioned issue, three other difficulties can be identified:
(i) there is a lack of measured data in the majorities of case studies;
(ii) there are significant uncertainties in measuring variables related to hydro-research;
(iii) even in gauged case studies, observed data exist usually in regular conditions, because either extreme conditions are rare or the extreme conditions have as consequence the failure of the measurement system.
Therefore, learning scientific models under these circumstances is at least problematic.

\paragraph{Scientific numerical modeling}
When learning models from data all scientist will come with their own bag of beliefs.
In this case we move from artificial science to scientific modeling, in which our beliefs are informed by data.
We discard models and hypothesis based on their ability to predict observations.

Scientific numerical modeling is the current dominant use of ML in hydro-research applications.
Modern numerical models can resolve fine spatial and temporal scales, mainly thanks to the exponential increase of computational power.
Concomitantly, although open challenges still exist, efforts to improve our fundamental understanding of hydro-sciences are decreasing, e.g. improving theories, collecting data, validating models, etc.

The most common use of ML is for model calibration (a.k.a. system identification), in which model's parameters are determined using measured data.
These parameters often enjoy a physical interpretation and this forms the basis for selecting or rejecting models.
Although ML offers a large variety of optimization and calibration tools, these are not frequently used in applied hydro-research, even in the cases in which observed data exist.
This might be partly due to the fact that the level of detail on fine-scaled models impose large runtime for each model evaluation, and optimization based on sampling becomes unfeasible.

\paragraph{Emulation}
In this component, instead of dealing with measured data, we use a simulator or model to sample input-output examples. 
With the data sampled from a known model we learn a new model which is numerically simpler than the original one.

Emulation is a subfield of ML and the intersection with scientific numerical modeling is given by
emulators that use prior scientific knowledge.
Not all methods of ML permit easy inclusion of this sort of prior knowledge, the best know to the authors are kernel methods (which includes many known ML methods).

Emulation is related to model order reduction (MOR)~\citep{morwiki}
, both produce fast surrogate models.
We distinguish the two based on the dimensionality of the output of the surrogate.
A MOR surrogate will generally provide as many states as the original simulator.
Emulators on the other hand provide outputs of smaller dimension,~\citep{Crutchfield2014}
 e.g. water depth at certain locations.
This makes emulators less general than reduced models, but this specification of the former generally renders them faster than the latter.
In practice the selection between reduced models or emulators is dictated by the application at hand and its engineering constraints (memory, processing power, accuracy, time budget, etc.)

The large runtime needed to sample fine-scaled models was already mentioned in the previous component.
Sampling for emulation needs to generate good enough emulators with sparse datasets, here is were a reduced output dimension and prior knowledge play a crucial role.
Once an emulator is learned, taking new samples becomes very cheap and optimizations technique for model calibration can be applied exhaustively, allowing not only model calibration but also real-time control, optimal design, uncertainty quantification, etc.

Emulation merges artificial science and scientific numerical modeling, with it we are able to discover hidden input-output relations while exploiting all available information about the phenomena.

\section{Emulation with prior knowledge}
Why is prior knowledge relevant? Sparse vs. Big data (prior knowledge irrelevant)

The sections below introduce the formal description of several scenarios encountered in applications
In all this scenarios we will denote the data set of inputs and outputs with $\bset{\bm{x},\bm{y}}$.
The hypotheses set, i.e. the set of functions or models used to represent the data, is denoted with $\mathcal{H}$

  \begin{figure}[htpb]
  \centering
    \begin{subfigure}[t]{0.3\textwidth}
      \begin{tikzpicture}
        \shade[inner color =blue!50, outer color=red, clip]
          (0,0) circle (2cm)
          node at (145:1.8cm) {$\mathcal{H}$};
          \coordinate (f_min) at (0,0);
          \draw[fill]
            (f_min) circle (1pt) node[right] at (f_min) {$\hat{f}$};
      \end{tikzpicture}
      \caption{\label{sfig:R} Regression}
    \end{subfigure}\\
    \begin{subfigure}[t]{0.3\textwidth}
      \begin{tikzpicture}
        \shade[inner color =blue!50, outer color=red, clip]
          (0,0) circle (2cm)
          node at (145:1.8cm) {$\mathcal{H}$};
          \begin{scope}[rotate=30]
            \draw[color=yellow, pattern=north west lines, pattern color=yellow]
              (-1cm,-2cm) rectangle (2cm,0)
              node at (-1.2cm,0){$h$};
          \end{scope}
          \draw
            (1cm,-2cm) arc (0:70:4cm)
            node at (50:0.6cm) {$g$};
          \begin{scope}[rotate=45]
            \draw[color=Plum]
              (0,-0.5cm) ellipse (2.5cm and 1cm)
              (-1.5cm,-0.2cm) ellipse (1.5cm and 0.25cm)
              node at (10:1.8){$\mathcal{D}$};
          \end{scope}
          \begin{scope}[rotate=38]
            \coordinate (f_min) at (0,-1.5cm);
            \draw[fill]
              (f_min) circle (1pt) node[right] at (f_min) {$\hat{f}$};
          \end{scope}
      \end{tikzpicture}
      \caption{\label{sfig:CR} constrained}
    \end{subfigure}
    \begin{subfigure}[t]{0.3\textwidth}
      \begin{tikzpicture}
        \shade[inner color =blue!50, outer color=red, clip]
          (0,0) circle (2cm)
          node at (145:1.8cm) {$\mathcal{H}$};
          \begin{scope}[rotate=30]
            \draw[color=yellow, pattern=north west lines, pattern color=yellow]
              (-1cm,-2cm) rectangle (2cm,0)
              node at (-1.2cm,0){$h$};
          \end{scope}
          \draw
            (1cm,-2cm) arc (0:70:4cm)
            node at (50:0.6cm) {$g$};
          \begin{scope}[rotate=45]
            \draw[color=Plum]
              (0,-0.5cm) ellipse (2.5cm and 1cm)
              (-1.5cm,-0.2cm) ellipse (1.5cm and 0.25cm)
              node at (10:1.8){$\mathcal{D}$};
          \end{scope}
          \coordinate (f_min) at (-0.1cm,0.25cm);
          \draw[fill]
            (f_min) circle (1pt) node[left] at (f_min) {$\hat{f}$};
          \draw[dashed,rotate=80] (0,-0.1cm) ellipse (0.4cm and 0.3cm);
      \end{tikzpicture}
      \caption{\label{sfig:RR} regularized}
    \end{subfigure}
    \begin{subfigure}[t]{0.3\textwidth}
      \begin{tikzpicture}
        \shade[inner color =blue!50, outer color=red, clip]
          (0,0) circle (2cm)
          node at (145:1.8cm) {$\mathcal{H}$};
          \begin{scope}[rotate=30]
            \draw[color=yellow, pattern=north west lines, pattern color=yellow]
              (-1cm,-2cm) rectangle (2cm,0)
              node at (-1.2cm,0){$h$};
          \end{scope}
          \draw
            (1cm,-2cm) arc (0:70:4cm)
            node at (50:0.6cm) {$g$};
          \begin{scope}[rotate=45]
            \draw[color=Plum]
              (0,-0.5cm) ellipse (2.5cm and 1cm)
              (-1.5cm,-0.2cm) ellipse (1.5cm and 0.25cm)
              node at (10:1.8){$\mathcal{D}$};
          \end{scope}
          \begin{scope}[rotate=30]
            \coordinate (f_min) at (0.4cm,-0.1cm);
            \draw[fill]
              (f_min) circle (1pt) node[right] at (f_min) {$\hat{f}$};
          \end{scope}
      \end{tikzpicture}
      \caption{\label{sfig:RCR} regularized \& constrained}
    \end{subfigure}

  \caption{\label{fig:}}
  \end{figure}

\subsection{The regression problem}
This is the situation without prior knwoledge or when the elements of the hypothesis set are built such that all prior knowledg eis already included into them (see section~\ref{sec:transformation} below)

  \begin{align}
    \hat{f} = \armin_{f \in \mathcal{H}} \, \Loss (f[\bm{x}], \bm{y}; \bm{\theta})
  \end{align}

For example, the wieghted sum of squares 
\begin{equation}
  \Loss (f[\bm{x}], \bm{y}; \bm{\theta}) := \sum_{i} \theta_i \left(f(\bm{x}_i) - y_i\right)^2
\end{equation}

\subsection{The constrained regression problem}
The constraints are used to intriduce knowledge abut the solution.
For example, we can ontroduce a (nonlinear) operator $\mathcal{D}$ which nullset
describes the admissible solutions, e.g. solutions to a differential equation.
The operator work son the eleements on the hypotheses set themsleves, it doesn't
use the actual data, that is it is a selection of elements on the hypotheses set.

  \begin{align}
    \hat{f} =& \armin_{f \in \mathcal{H}} \, \Loss (f[\bm{x}], \bm{y}; \bm{\theta})
      \\\textrm{s. t.}& \nonumber\\
      &\mathcal{D}(f; \bm{\mu}) = 0\\
      &g(f; \bm{\mu}) = 0\\
      &h(f; \bm{\mu}) \leq 0
  \end{align}

\subsection{The regularized regression problem}
  \begin{multline}
    \hat{f} = \armin_{f \in \mathcal{H}} \, \Loss (f[\bm{x}], \bm{y}; \bm{\theta})
       + \kappa_D \Vert \mathcal{D}(f; \bm{\mu}) \Vert
       + \kappa_g \Vert g(f; \bm{\mu}) \Vert
       \\ + \kappa_h \Vert m(h(f; \bm{\mu})) \Vert
  \end{multline}

\subsection{The regularized and constrained Regression problem}
  \begin{align}
    \hat{f} =& \armin_{f \in \mathcal{H}} \, \Loss (f[\bm{x}], \bm{y}; \bm{\theta})
              + \kappa_D \Vert \mathcal{D}(f; \bm{\mu}) \Vert
      \\ \textrm{s. t.}& \nonumber \\
      &g(f; \bm{\mu}) = 0\\
      &h(f; \bm{\mu}) \leq 0
  \end{align}

\subsection{Simulations with variability: no interpolation}
Effects of discretizations: different meshes different results

Stochastic simulators: Cite Garamani

Effective models: turbulence models

Including data with simulations

\subsection{Invariant kernsl, chang eof variables}
Some ML methods accept the introduction of equality ocnstratins, e.g. invarinat kernels, cite David Ginsburg

\subsection{Smaller hypotheses sets are better}
VC dimension, etc...

\section{Applications in Hydrology \& Hydraulics}
Most simulators in hydroresearch are based on deterministic differential equations (DE) describing the behavior of fluids and their interaction with the environment.
When performing emulation the target function is the response of the simulator to all physically meaningful inputs.
That is the hypothesis set contains all possible simulator outputs (admissible solutions to the DE), and the best available knowledge we can provide to our emulator is the DE itself.
The training examples are a dataset of simulations corresponding to a representative set of inputs.

%Depending on the situation we might have even more information available for our emulator: symmetries, conservation laws, etc.
%How to effectively include all types of prior information into emulator construction is a fruitful and still open research field.

In this scenario the learning algorithm can be implemented as the search of a function in the hypothesis set that solves an optimization problem, e.g.:

\begin{equation}
f = \armin_{g \in \mathcal{H}} \lambda_1 \sum \left(y_i - g(x_i)\right)^2 + \lambda_2 ||\mathcal{D}_\theta g(x)||^2\label{eq:opt_problem}
\end{equation}

\noindent where $\mathcal{D}_\theta$ is the differential operator with parameters $\theta$.
Eq.~\eqref{eq:opt_problem} states that the function $f$ approximates the data $(x_i,y_i)$ and it is "almost" a response of our simulator (e.g. Shallow water equations).
%\begin{equation}
%\mathcal{D}_\theta g(x) = 0
%\end{equation}
%The first term in~\eqref{eq:opt_problem} measures the distance between the data and the sought function at observed points $f(x_i)$ is measured in the Euclidean sense, and $f$ is close to the nullity of the differential operator.
There are several ways of looking at this problem: i) the first term is a constraint (is required to be exactly zero), ii) the second term is a constraint, iii) none of the terms is a constraint, or iv) both terms are constraints ($f$ realizes a zero of the cost function). 
Case i) corresponds to interpolation and is very relevant in emulation.
Case ii) searches for a response of our simulator $f$ that approximates the data~\citep{Poggio1990}.
Case iii) is a regularized regression problem~\citep{Ramsay2017, azzimonti12}.
And finally, case iv) gives a solution that interpolates the data and is a repsonse of our simulator (this might not be always possible)~\citep{}

\begin{align}
2 c \pder{h}{t} + \pder{Q}{x} &= 0\\
\pder{Q}{t} + 2 \frac{Q}{A} \pder{Q}{x} - \left(2 \frac{Q^2}{A^2} c + g A\right)\pder{h}{x} + gA\left[S_f(Q,h) - S(x)\right] &= 0
\end{align}

\begin{figure}[htpb]
  \includegraphics[width=0.9\textwidth]{stvenant_schema.png}
  \caption{\label{fig:stvenant}}
\end{figure}

\paragraph{Acknowledgements}
We thank Dr. Jörg Rieckermann for his continuous support.
This research has received funding from Eawag's discretionary funding program (project EmuMore \url{kakila.bitbucket.io/emumore}).

\sloppy
\bibliographystyle{unsrtnat}
\bibliography{References}

\end{document}
