\documentclass[10pt,english,final,a4paper]{article}
\usepackage[usenames,dvipsnames,svgnames]{xcolor}
\usepackage{hyperref} % Hyperlinks within and outside the document
\hypersetup{
unicode=false,          % non-Latin characters in Acrobat’s bookmarks
pdfauthor={JuanPi Carbajal},%
pdftitle={Mechanistic Emulator},%
colorlinks=true,       % false: boxed links; true: colored links
linkcolor=OliveGreen,          % color of internal links
citecolor=Sepia,        % color of links to bibliography
filecolor=magenta,      % color of file links
urlcolor=NavyBlue,          % color of external links
}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage{lmodern} % German related symbols
\usepackage{fouriernc} % Use the New Century Schoolbook font, comment this line to use the default LaTeX font or replace it with another

\usepackage{siunitx}
%\sisetup{mode=text,range-phrase = {\text{~to~}}}
\sisetup{
  range-phrase = {,}\ ,
  range-units  = brackets,
  open-bracket = [,
  close-bracket= ],
}

%\usepackage{hyperxmp}
%\usepackage[
%type={CC},
%modifier={by-sa},
%version={4.0},
%imagewidth={2cm},
%]{doclicense}
%\usepackage{mited} % Highlight source code using Pygments
%\definecolor{LightGray}{gray}{0.95}
%\newcommand{\octcli}[1]{
%\mint[bgcolor=LightGray]{octave}|octave> #1|
%}
%\newcommand{\octv}[1]{
%\mintinline{octave}{#1}
%}

\usepackage[numbers, sort&compress]{natbib}

\usepackage{babel}
\usepackage{graphicx}
\usepackage[small]{caption}
\usepackage{bm}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{thmtools}
\usepackage[mathscr]{eucal}
\DeclareMathAlphabet{\mathcal}{OMS}{cmsy}{m}{n}

\usepackage{authblk}
\usepackage{multirow}

%\usepackage{pgfplots}
%\pgfplotsset{every axis/.append style={line width=1pt}}

\usepackage{subcaption} % subfigures

\usepackage{enumitem}


\graphicspath{{img/}}

% Customs commmands
% Calculus
\newcommand{\ud}{\mathrm{d}}
\newcommand{\pder}[2]{\frac{\partial{#1}}{\partial{#2}}}
\newcommand{\dpder}[2]{\frac{\partial^2{#1}}{\partial{#2^2}}}
\newcommand{\sderp}[3]{\frac{\partial^2{#1}}{\partial{#2}\partial{#3}}}
\newcommand{\tder}[2]{\frac{\ud{#1}}{\ud{#2}}}
\newcommand{\defitg}[4]{\int_{#1}^{#2} {#3}\ud {#4}}
\newcommand{\itg}[2]{\int {#1}\ud {#2}}

% Vectors
%\newcommand{\bm}[1]{\boldsymbol{#1}}
\newcommand{\bmm}[1]{\bm{#1}}
\newcommand{\bra}[1]{\left\langle  {#1}\right|}
\newcommand{\ket}[1]{\left|{#1}\right\rangle}
\newcommand{\braket}[2]{\left\langle {#1} | {#2} \right\rangle}
% Logicals
\newcommand{\suchthat}{\big \backslash \;}
% Others
\newcommand{\ini}[1]{{}_0#1}
\newcommand{\eval}[1]{\Big |_{#1}}
\newcommand{\bset}[1]{\big\lbrace {#1} \big\rbrace}
\newcommand{\stimes}[2]{{#1}\!\times\!{#2}}
\newcommand{\trp}{\top}
\newcommand{\preup}[2]{{}^{#2}\!{#1}}
\newcommand{\subm}[2]{\scriptscriptstyle{#1},\scriptscriptstyle{#2}}
\newcommand{\trset}[1]{{#1}_\text{\tiny trn}} % mean function
\newcommand{\tsset}[1]{{#1}_\text{\tiny tst}} % mean function

% Operators
\DeclareMathOperator*{\err}{err}
\DeclareMathOperator*{\armin}{arg\,min}
\DeclareMathOperator*{\armax}{arg\,max}
\DeclareMathOperator*{\linspan}{span}
\DeclareMathOperator*{\rank}{rank}
\DeclareMathOperator*{\cov}{cov}
\DeclareMathOperator*{\sign}{sign}
\DeclareMathOperator*{\GP}{\mathcal{G}\!\mathcal{P}} % Gaussian process
\DeclareMathOperator*{\gaussian}{\mathcal{N}} % Gaussian distribution
\DeclareMathOperator*{\meanf}{\mathfrak{m}} % mean function
\DeclareMathOperator*{\covf}{\mathfrak{K}} % mean function
\DeclareMathOperator*{\Gf}{\bmm{G}} % Linear dynamical system's Green's function.
\DeclareMathOperator*{\Gfa}{\bmm{G}^{\dagger}} % Adjoint Green's function.

% Local for this document
\newcommand{\szp}{m} % dimension of local proxy
\newcommand{\szP}{M} % dimension of aggregated proxy's phase space
\newcommand{\nT}{N}  % number of time samples
\newcommand{\nD}{n}  % number of design datasets (parameter values)
\newcommand{\nO}{M^\prime}  % number of observed dimensions
\newcommand{\szK}{\szP\nT}
\newcommand{\Kf}[2]{K\left({#1},{#2}\right)} % Covariance function of emulator.
\newcommand{\Sf}[1]{\Sigma_{#1}} % Covariance function of coupling noise.
\newcommand{\pl}{\bm{\theta}} % proxy parameter vector.
\newcommand{\pnl}{\bm{\gamma}} % nl simulator parameter vector.
\newcommand{\pnlc}[1]{\gamma_{#1}} % nl simulator parameter vector component .
\newcommand{\plc}[1]{\theta_{#1}} % nl simulator parameter vector component .
\newcommand{\Pl}{\Theta} % Array of proxy parameter vector (emulator parameter vector).
\newcommand{\Pnl}{\Gamma} % Array of nonlinear model parameter vector.
\newcommand{\LO}[1]{\operatorname{L}_{#1}} % Linear differential operator.
%\newcommand{\heavi}[1]{\Xi\left({#1}\right)} % Heaviside function.
\newcommand{\heavi}{\mathcal{H}}

% Comments
\newcommand{\jpi}[1]{{\color{Magenta} JPi: #1}}

% highlight
\newcommand{\hl}[2]{\colorbox{#1}{#2}}
% Problem section
\newtheorem{problem}{Problem}

% Citation aliases
\defcitealias{Octave}{GNU Octave}
\defcitealias{Inkscape}{Inkscape}
\defcitealias{Sage}{Sage}

\begin{document}

\begin{gather}
  \pder{\mathcal{A}}{\tau} + \pder{\mathcal{Q}}{\xi} = 0\\
  \pder{\mathcal{Q}}{\tau} + \pder{}{\xi}\frac{\mathcal{Q}^2}{\mathcal{A}} + g\mathcal{A}\pder{\mathcal{H}}{\xi} + g\mathcal{A}(S_f - S_z) = 0
\end{gather}

\begin{align}
  \xi &= L x\\
  \tau &= T t\\
  \mathcal{H}(\xi,\tau) &= H h(x,t)\\
  \mathcal{A}(\mathcal{H}) &= A a(h)\\
  \mathcal{Q}(\xi,\tau) &= Q q(x,t)
\end{align}
$L$ length of segment.
$H$ maximum level of segment.
$A$ total cross sectional area of segment.
$Q$ and $T$ are free scalings to be defined.

First equation
\begin{equation}
  \frac{A}{T}\pder{a}{t} + \frac{Q}{L}\pder{q}{x} = 0
\end{equation}
\begin{equation}
  \pder{a}{t} + \frac{T}{LA}Q\pder{q}{x} = 0
\end{equation}

Second equation
\begin{equation}
  \frac{Q}{T}\pder{q}{t} + \frac{Q^2}{LA}\pder{}{x}\frac{q^2}{a} + \frac{gAH}{L} a\pder{h}{x} + + gA(S_f - S_z)a = 0
\end{equation}

Take
\begin{equation}
  Q = \frac{LA}{T}
\end{equation}

\begin{equation}
  \frac{LA}{T^2}\pder{q}{t} + \frac{LA}{T^2}\pder{}{x}\frac{q^2}{a} + \frac{gAH}{L} a\pder{h}{x} + gA(S_f - S_z)a = 0
\end{equation}

\begin{equation}
  \pder{q}{t} + \pder{}{x}\frac{q^2}{a} + T^2\frac{g}{L}\frac{H}{L} a\pder{h}{x} + T^2\frac{g}{L}(S_f - S_z)a = 0
\end{equation}

Take
\begin{equation}
  T^2 = \frac{L}{g}
\end{equation}

\begin{equation}
\pder{q}{t} + \pder{}{x}\frac{q^2}{a} + \frac{H}{L} a\pder{h}{x} + (S_f - S_z)a = 0
\end{equation}

Finally
\begin{gather}
  \pder{a}{t} + \pder{q}{x} = 0\\
  \pder{q}{t} + \pder{}{x}\frac{q^2}{a} + S_H a\pder{h}{x} + (S_f - S_z)a = 0
\end{gather}

\begin{align}
S_f &= f_D \frac{1}{2g} \frac{\mathcal{P}\mathcal{Q}^2}{4\mathcal{A}^3} = \frac{f_D}{8} \frac{PQ^2}{gA^3} \frac{pq^2}{a^3} = \frac{f_D}{8} \frac{PL}{A} \frac{pq^2}{a^3} = \frac{f_D}{8S_R} \frac{pq^2}{a^3}\\
S_R &= \frac{A}{P}\frac{1}{L}\\
S_z &= - \frac{\Delta z}{L}\\
S_H &= \frac{H}{L}
\end{align}
$P$ total perimeter of the segment.
$\Delta z$ change of height along the segment

\section{Terms re-arranged}
\begin{equation}
  \pder{q}{t} + \frac{2q}{a}\pder{q}{x} - \frac{q^2}{a^2}\pder{a}{x} + S_H a\pder{h}{x} + (S_f - S_z)a = 0
\end{equation}

Cross-section $a(h)$ is given by z-symmetric curve $c(z)$ (the same along the whole segment)
\begin{align}
a &= 2\int_0^{h} c(z) \ud z\\
\tder{a}{h} &= 2 c(h)
\end{align}

\begin{equation}
  \pder{q}{t} + \frac{2q}{a}\pder{q}{x} - \frac{q^2}{a^2}\tder{a}{h}\pder{h}{x} + S_H a\pder{h}{x} + (S_f - S_z)a = 0
\end{equation}

avoid singularity at $h = 0$

\begin{equation}
  a^2\pder{q}{t} + 2qa\pder{q}{x} - q^2\tder{a}{h}\pder{h}{x} + S_H a^3\pder{h}{x} + (S_f - S_z)a^3 = 0
\end{equation}


\begin{gather}
  2c\pder{h}{t} + \pder{q}{x} = 0\\
  a^2\pder{q}{t} + 2qa\pder{q}{x} + \left(S_H a^3 - 2q^2c\right) \pder{h}{x} + (S_f - S_z)a^3 = 0
\end{gather}

\subsection{As vector equation}
\begin{align}
  c\pder{h}{t} & = - \frac{1}{2}\pder{q}{x}\\
  a^2\pder{q}{t} & = - 2qa\pder{q}{x} + \left(2q^2c - S_H a^3\right) \pder{h}{x} + (S_z - S_f)a^3
\end{align}

Define
\begin{equation}
y = \begin{bmatrix}h\\q\end{bmatrix}
\end{equation}

\begin{equation}
\begin{bmatrix}c & 0\\ 0 & a^2 \end{bmatrix} \pder{y}{t} = \begin{bmatrix}0 & -\frac{1}{2} \\ 2q^2c - S_H a^3 & - 2qa\end{bmatrix} \nabla y + \begin{bmatrix}0 \\ (S_z - S_f)a^3\end{bmatrix}
\end{equation}

\begin{equation}
M(y) \pder{y}{t} = K(y) \nabla y + f(y)
\end{equation}

$M$ is nonsingular for $h \neq 0$

\begin{align}
\pder{y}{t} = A(y) \nabla y + f(y)\\
A(y) = M^{-1}(y) K(y) = \begin{bmatrix}0 & -\frac{1}{2c} \\ 2\frac{q^2}{a^2}c - S_H a & - 2\frac{q}{a}\end{bmatrix}
\end{align}

\begin{align}
V\pder{y}{t} &= VAV^{-1}V\nabla y + V f(y) = D\, V\nabla y + V f(y)\\
D &= \begin{bmatrix}\frac{q}{a} - k & 0 \\ 0 & k - \frac{q}{a} \end{bmatrix}\\
V &= \frac{\sqrt{2}}{2k}\begin{bmatrix}k - \frac{q}{a} & \frac{1}{2c}\\ k + \frac{q}{a} & -\frac{1}{2 c}\end{bmatrix}\\
k &= \sqrt{S_H\frac{a}{2c}}
\end{align}
\end{document}
