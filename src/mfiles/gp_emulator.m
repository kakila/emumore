## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [m dm2] = gp_emulator (HYP, POST, ARGS, x)
  [N dim] = size (x);

  no_parallel = ifelse (exist ('parcellfun'), false, true);
  func = @(h,p) gp(h,ARGS{:},p,x);
  if iscell (HYP)
    if no_parallel
      [m dm2] = cellfun (func, HYP, POST, 'UniformOutput', false);
    else
      [m dm2] = parcellfun (nproc, func, HYP, POST, 'UniformOutput', false);
    endif
    m   = cell2mat (m);
    dm2 = cell2mat (dm2);
  else
    [m dm2] = func(HYP, POST);
  endif

endfunction
