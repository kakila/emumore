## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function  [HYP, POST, ARGS, NLML] = gpcell (x, y, cf, varargin)

  [N dimx] = size (x);
  [N dimy] = size (y);

  #### Parse input values
  parser = inputParser ();

  parser.addParamValue ('hyperparams', [], @(x)isempty(x)|isstruct(x));

  parser.addParamValue ('mean', @meanZero, ...
                         @(x)is_function_handle(x)|iscell(x));
  parser.addParamValue ('likelihood', @likGauss, ...
                         @(x)is_function_handle(x)|iscell(x));
  parser.addParamValue ('inference', @infExact, ...
                         @(x)is_function_handle(x)|iscell(x));

  parser.addParamValue ('Niter', -Inf, @isscalar);

  parser.addSwitch ('verbose');

  parser.parse (varargin{:});
  res = parser.Results;
  #####

  # make all handle cells
  if ~iscell (cf); cf = {cf}; endif

  mf = res.mean;
  if ~iscell (mf); mf = {mf}; endif
  lf = res.likelihood;
  if ~iscell (lf); lf = {lf}; endif
  infe = res.inference;
  if ~iscell (infe); infe = {infe}; endif

  # obtain dimension of hyperparameters
  D      = dimx;
  dimcov = eval (feval (cf{:}));
  dimmea = eval (feval (mf{:}));
  dimlik = eval (feval (lf{:}));

  # default hyper-p values
  if isempty (res.hyperparams)
    res.hyperparams = struct ( 'cov', zeros (dimcov, 1), ...
                              'mean', zeros (dimmea, 1), ...
                               'lik', zeros (dimlik, 1));
  endif

  ARGS{1} = res.Niter;
  ARGS{2} = infe;
  ARGS{3} = mf;
  ARGS{4} = cf;
  ARGS{5} = lf;

  if ~iscell (y) & dimy > 1
    y = mat2cell (y, N, ones (1, dimy));
  endif

  no_parallel = ifelse (exist ('parcellfun'), false, true);
  verbose     = ifelse (no_parallel, res.verbose, false);

  func = @(z) gp_fit (x, z, res.hyperparams, ARGS, verbose);

  if iscell (y)
    if no_parallel
      [HYP, POST, NLML] = ...
                cellfun (func, y, 'UniformOutput', false);
    else
      [HYP, POST, NLML] = ...
                parcellfun (nproc, func, y, 'UniformOutput', false);
    endif

  else
    [HYP, POST, NLML] = func (y);
  endif

  ARGS = {ARGS{2:end},x};
endfunction

function [hyp, post, nlml] = gp_fit (x, y, h, p, v = false)
  % Optimize hyperparamters
  s = evalc ('hyp = minimize (h, @gp, p{:}, x, y);');
  if v
    s = strjoin (strsplit (s)(end-5:end-1));
    printf ("%s\n", s);
    fflush (stdout);
  endif
  % Assemgle GP post structure
  [nlml,~,post] = gp (hyp, p{2:end}, x, y);
endfunction
