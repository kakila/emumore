## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [x y idx varargout] = inputselect_greedy (X, Y, criteria, tol, ...
                               varargin)
  [n dim]   = size (X);
  ################################
  # Parse inputs
  parser = inputParser ();
  parser.addParamValue ('Niter', 50, @isscalar);
  parser.addParamValue ('Nmax', n, @isscalar);
  parser.addParamValue ('Nrsv', ceil (0.1*n), @isscalar);
  parser.addParamValue ('Nini', 1, @isscalar);
  parser.addSwitch ('verbose');
  parser.parse (varargin{:});
  opt = parser.Results;
  clear parser;
  ################################
  # check tol is right
  if length (tol) < 2
    error ('Octave:invalid-input-arg', 'I need two tolerances!');
  end%if

  # Maximum obsrvations is datalength
  opt.Nmax      = min (opt.Nmax, n);

  y = x = idx = struct ('trn', [], 'tst', [], 'rsv', []);
  # Reserve data
  idx.rsv = randperm (n, opt.Nrsv).';
  x.rsv   = X(idx.rsv,:);
  y.rsv   = Y(idx.rsv);
  ind     = setdiff ((1:n).', idx.rsv);

  #####
  # Initalization
  printf ('Assembling inital set ...\n'); fflush (stdout);
  [~,trn] = max (X(ind,:));
  [~,tmp] = min (X(ind,:));
  trn     = [trn(:); tmp.'];
  tmp     = convhull (1:length(ind),Y(ind));
  trn     = unique ([trn; tmp]);
  if length (trn) < opt.Nini
    dn = setdiff (1:length(ind), trn);
    trn = [trn; dn(randperm(length(dn),opt.Nini-length(trn)))(:)];
  end%if

  [XYtrn, XYtst, ~, tst] = splitdata ([X(ind,:), Y(ind,:)], trn);

  x.trn   = XYtrn(:,1:end-1);
  x.tst   = XYtst(:,1:end-1);
  y.trn   = XYtrn(:,end);
  y.tst   = XYtst(:,end);
  Ntrn    = length (y.trn);
  idx.tst = ind(tst);
  idx.trn = ind(trn);

  return_val = nargout > 3;
  if opt.verbose
    figure(1);
    clf
    herr  = plot (Ntrn, NA,'-ok', Ntrn, NA,'or', Ntrn, NA, '-ob');
    set (herr(2), 'markerfacecolor', 'r');
    grid on
    ylabel ('Criteria value')
    xlabel (' # observations')
    htol = line ([Ntrn opt.Nmax],[tol(1) tol(1)]);
  end%if

  done = false;
  zval_prev = Inf;
  # find out type of criteria
  # k(1) from criteria is used to stop the iteration
  # k(2) from criteria is used to select new points to add
  [~, zval] = criteria (x.trn, y.trn, x.rsv, y.rsv, 0);
  k = size (zval);
  passed =@(x,y) [x<tol(1) y<tol(2)];

  while Ntrn < opt.Nmax

    [tmp val] = criteria (x.trn, y.trn, x.tst, y.tst, opt.Niter);
    [~, zval] = criteria (x.trn, y.trn, x.rsv, y.rsv, 0);
    zval = zval (k(1));
    val  = val (k(1));
    slc  = tmp (:,k(2));
    tol2 = 0.05;
    dval = abs ((zval-zval_prev)/zval_prev);
    if any (passed (zval, dval))
      done = true;
    end%if
    zval_prev = zval;

    if return_val
      varargout{1} = [varargout{1}; zval];
    end%if

    if opt.verbose
      if isna (get(herr(2),'ydata'))
        yerr = val(1);
        xerr = Ntrn;
        zerr = zval(1);
      else
        xerr = [get(herr(1),'xdata') Ntrn];
        yerr = [get(herr(1),'ydata') val(1)];
        zerr = [get(herr(3),'ydata') zval(1)];
      end%if
      set (herr(1), 'xdata', xerr, 'ydata', yerr);
      set (herr(2), 'xdata', xerr(end), 'ydata', yerr(end));
      set (herr(3), 'xdata', xerr, 'ydata', zerr);
      set (htol,'xdata',xerr([1 end]));
      drawnow();
      printf ("Criteria 1: %g (%g)\n", zval, val);
      printf ("Criteria 2: %g\n", dval);
      fflush (stdout);
    end%if

    if done

      if opt.verbose
        tmp = find (passed (val, dval));
        fmt = "%d";
        if length (tmp) == 2
          fmt = [fmt " & %d"];
        end%if
        printf (["Stopped due to tolerance " fmt "\n"], tmp); fflush (stdout);
      end%if
      break
    end%if

    trn                           = [trn; tst(slc)(:)];
    [XYtrn, XYtst, ~, tst] = splitdata ([X(ind,:), Y(ind,:)], trn);

    x.trn   = XYtrn(:,1:end-1);
    x.tst   = XYtst(:,1:end-1);
    y.trn   = XYtrn(:,end);
    y.tst   = XYtst(:,end);
    Ntrn    = length (y.trn);

  end%while
  if opt.verbose && Ntrn >= opt.Nmax
    printf ("Stopped due to Nmax %d\n", Ntrn); fflush (stdout);
  end%if

  idx.tst                       = ind(tst);
  idx.trn                       = ind(trn);

end%function
