## Copyright (C) 2016 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: JuanPi Carbajal <ajuanpi+dev@gmail.com>

function [q h] = logSPM (t, u, varargin)
  ######################
  ## Parse parameters ##
  parser = inputParser ();
  parser.FunctionName = 'logSPM';

  # Covariance and mean of dynamical system
  parser.addParamValue ('k', 0.02);       # Exponent controlling saturated area fraction of soil
  parser.addParamValue ('sF', 2300);      # Factor controlling saturated area fraction of soil
  parser.addParamValue ('ssfMax', 0.62);  # Subsurface stormflow at full saturation [mm/day]
  parser.addParamValue ('rgeMax', 5.6);   # Groundwater recharge rate at full saturation [mm/day]
  parser.addParamValue ('kBf', 6.3e-5);   # Groundwater discharge constant
  parser.addParamValue ('kStream', 0.47); # Stream discharge constant
  parser.addParamValue ('rMult', 1.21);   # Observed storm depth rainfall multiplier
  parser.addParamValue ('pet', 0);        # Evapotranspiration multiplier
  parser.addParamValue ('init', [0 0 0]); # Inital reservoir levels

  parser.parse (varargin{:});

  param = parser.Results;
  ######################

  try
    opt = odeset ("MaxStep", t(2)-t(1), "InitialStep", (t(end)-t(1))/100);
    [t,h] = ode45 (@(t,x)ode_logspm(x,t,u,param), t, param.init);
  catch
    h = lsode (@(x,t)ode_logspm(x,t,u,param), param.init, t);
  end

  q = param.kStream * h(:,3);

endfunction

function dydt = ode_logspm (y, t, r, p)

  rain = p.rMult * r(t);
  # The soil store with depth s = y(1)
  et    = p.pet * (1 - exp (-y(1,:)));                                   # actual evapotranpiration flux
  f     = 1 ./ (1 + (p.sF + 99) * exp(-p.k * y(1,:))) - 1./(p.sF + 100); # saturation-soil depth function
  rge   = f * p.rgeMax;                                                  # groundwater recharge flux
  ssf   = f * p.ssfMax;                                                  # subsurface stormflow flux
  quick = f * rain;                                                      # quickflow flux

  dydt(1,:)  = rain - quick - ssf - rge - et;

  # Groundwater store: linear reservoir with depth h = y(2)
  bf         = p.kBf * y(2,:);                                           # baseflow flux
  dydt(2,:)  = rge - bf;

  # Stream store: linear reservoir with depth S = y(3);
  q          = p.kStream * y(3,:);                                       # stream runoff flux
  dydt(3,:)  = quick + ssf + bf - q;

endfunction
