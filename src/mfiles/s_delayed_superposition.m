# Copyright (C) 2017 - Juan Pablo Carbajal
#
# This progrm is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#

## Superposition of delayed function
# In this script the explore the result of the superposition
# of a delayed waveforms $\varphi(t)$, i.e.
#
# $$ f(t, \vec{\tau}) = \sum_{i=1}^N \varphi(t - \tau_i) $$
#
# where the delays $\lbrace\tau_i\rbrace \geq 0$ form the delay array.
#
# For given delay array the sum can always be written in the form
#
# $$ f(t) = \sum_{\tau} \varphi(t - \tau) \#(\tau) $$
#
# with $\#(\tau)$ the frequency of occurrence of delay $\tau$.
#
# When $N \to \infty$ the solution converges to
#
# $$ f_\infty(t) = N \int_{\tau_m}^{\tau_M} \varphi(t-s) p(s) ds$$
#
# where $p(s)$ is the distribution of the delay array.
# This coincides with the sample mean, when infite samples are taken.
# The maximum difference between $f_\infty$ and $f(t)$ increases with smaller
# delay arrays.

##
# To simplify the structure of the script we define several varibales that will be
# used through out the script
T    = 1;                     # Time span
nT   = 100;                   # Numer of time samples
t    = linspace (0, 1, nT).'; # Time vector

##
# Here we choose the waveform, we are particularly interested in waveforms whose
# support is included in the interval $[0,T]$, or with waveforms of unbounded support
# but fast decay, e.g. a Gaussian waveform or functions in $L_2([0,T])$.
s   = 0.02;
phi = @(t) exp (-t.^2/(2*s^2));

## Uncorrelated delays
# We take uniform distributed delays and weights in some bounded interval.
N       = 100;
figure (1);
clf
d_m = 0.2; d_M = 0.8;
w_m = 1; w_M = 0.1*N;
pdf_d = @(s) unifpdf (s, d_m, d_M);
pdf_w = @(s) unifpdf (s, w_m, w_M);

hold on
for i = 1:100
  d = unifrnd (d_m, d_M, 1, N); # Uniformly distributed
  w = unifrnd (w_m, w_M, N, 1); # Uniformly distributed
  f = phi (t - d) * w;
  h = plot (t, f, '-k');
endfor

hphi_d = arrayfun (@(y) quadgk (@(s) phi (y-s) .* pdf_d(s), d_m, d_M), t);
w_avg  = quadgk (@(s) s .* pdf_w(s), w_m, w_M);
f_avg  = w_avg * N * hphi_d;

h(2)  = plot (t, f_avg, '-r', 'linewidth', 2);
axis tight
hold off
legend (h, {'samples', '<f>'})
ylabel ('f(t)')

##
# We now consider beta distributed delays
# and narrow uniform weights
d_a = 2  ; d_b = 5;
w_m = 0.9; w_M = 1.1;
pdf_d = @(s) betapdf (s, d_a, d_b);
pdf_w = @(s) unifpdf (s, w_m, w_M);

figure (2);
clf
hold on
for i = 1:100
  d     = betarnd (d_a, d_b, 1, N); # Beta distributed
  w     = unifrnd (w_m, w_M, N, 1); # Uniformly distributed
  f     = phi (t - d) * w;
  h     = plot (t, f, '-k');
endfor

hphi_d = arrayfun (@(y) quadgk (@(s) phi (y-s) .* pdf_d(s), 0, inf), t);
w_avg  = quadgk (@(s) s .* pdf_w(s), w_m, w_M);
f_avg  = w_avg * N * hphi_d;

h(2)  = plot (t, f_avg, '-r', 'linewidth', 2);
axis tight
hold off
legend (h, {'samples', '<f>'})
ylabel ('f(t)')
