# Copyright (C) 2017 - Juan Pablo Carbajal
#
# This progrm is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#

## Superposition of a time-scaled and delayed waveform
# In this script the explore the result of the superposition
# of a time-scaled and delayed waveforms $\varphi(t)$, i.e.
#
# $$ f(t, \vec{\lambda}) = \sum_{i=1}^N \varphi(\begin{bmatrix}t & 1\end{bmatrix} \vec{\lambda}^\top_i) $$
#
# The sample mean, when infite samples of paramters are taken is.
#
# $$ f_\infty(t) = N \int_{a}\int_{b} \varphi(a t + b) p(a,b) dbda$$
#
# where $p(a,b)$ is the joint distribution of the parameters.

##
# To simplify the structure of the script we define several variables that will be
# used through out the script
T    = 1;                     # Time span
nT   = 100;                   # Numer of time samples
t    = linspace (0, 1, nT).'; # Time vector

##
# Here we choose the waveform, we are particularly interested in waveforms whose
# support is included in the interval $[0,T]$, or with waveforms of unbounded support
# but fast decay, e.g. a Gaussian waveform or functions in $L_2([0,T])$.
s   = 0.02;
phi = @(t) exp (-t.^2/(2*s^2));
a_span = {1, 10};    # Scalings (>1 we consider only dilations)
b_span = {0.1, 0.8}; # Delays

##
# We take log-Gaussian distributed parameters in some bounded interval.
# The hyper-paramters are estimated from a reasonable range of values
a_par  = {mean([a_span{:}]), std([a_span{:}])};
a_lpar = {log(a_par{1}), sqrt(log(a_par{2}/a_par{1}^2 + 1))};
b_par  = {mean([b_span{:}]), std([b_span{:}])};
b_lpar = {log(b_par{1}), sqrt(log(b_par{2}/b_par{1}^2 + 1))};
#pdf_ab = @(a,b) lognpdf (a, a_lpar{:}) .* lognpdf (b, b_lpar{:});

##
# These are the seed functions for the mean
# The "analytic" formula is commented out becasue it takes too long to integrate.
# We integrate using a simple Monte Carlo method.

#hphi   = @(t,p) dblquad (@(a,b) phi((t - b) ./ a) .* p (a, b), ...
#                           1, Inf, 0, Inf, 1e-1,'quadgk');
#hphirt = @(t,r,p) dblquad (@(a,b) ...
#                            phi((r - b) ./ a) .* phi((t - b) ./ a) .* p (a, b), ...
#                            1, Inf, 0, Inf, 1e-1,'quadgk');
#tic
#hphi_ab = arrayfun (@(y) hphi(y,pdf_ab), t);
#toc
#f_avg   = N * hphi_ab;
tic
Nmc   = 1e5;
a     = lognrnd (a_lpar{:}, 1, Nmc);
b     = lognrnd (b_lpar{:}, 1, Nmc);
tab   = (t - b) ./ a;
Pab   = phi(tab);
hphi  = ( Pab * ones (Nmc, 1) ) / Nmc;
hphi2 = ( Pab * Pab.' ) / Nmc;
toc

N     = 30;
f_avg = N * hphi;
K     = N * (hphi2 - hphi.*hphi.');

figure (1);
clf
hold on
f_m = zeros (nT, 1);
tic
grey = 0.7*ones(1,3);
for i = 1:100
  a   = lognrnd (a_lpar{:}, 1, N);
  b   = lognrnd (b_lpar{:}, 1, N);
  tab = (t - b) ./ a;
  f   = sum (phi (tab), 2);
  h   = plot (t, f, '-', 'color', grey);
endfor
toc

sd   = sqrt (diag (K));
h(2) = fill ([t; flipud(t)], [f_avg-sd; flipud(f_avg+sd)], 0.2*ones(1,3), ...
             'edgecolor', 'None');
h(3) = plot (t, f_avg, '-r', 'linewidth', 2);
axis tight
hold off
legend (h([1 3]), {'samples', '<f>'})
ylabel ('f(t)')

## Inference
# In this section we implement two ways of inferring
# the numer of delays based on a finite set of observations
# and information about the waveform and the distribution
# of delays.
#
# First, we calculate the number of delays by fitting the
# mean function to the observed data.
# We then use a Gaussian process to do the inference.
#

nTo = 15;                            # Number of observations
io  = round (linspace (1,nT,nTo)).'; # Indexes

a   = lognrnd (a_lpar{:}, 1, N);     # Scalings
b   = lognrnd (b_lpar{:}, 1, N);     # Delays
tab = (t - b) ./ a;
y   = sum (phi (tab), 2);            # Full data

to    = t(io);                         # Observation times
yo    = y(io);                         # Observations
sy    = 0.1;                           # Observation noise
yo    = yo + sy * randn (nTo,1);       # Noisy obs.

##
# First we consider the case of perfect information
# Gaussian process estimation of number of delays,
# i.e. maximum marginal likelihood solution

function [n hphi gp] = gp_estim (phi, t, io, yo, arnd, brnd, sy = sqrt(eps))

  # Monte Carlo
  Nmc   = 1e5;
  a     = arnd (Nmc);
  b     = brnd (Nmc);
  tab   = (t - b) ./ a;
  Pab   = phi(tab);

  hphi  = ( Pab * ones (Nmc, 1) ) / Nmc;

  hphi2  = ( Pab * Pab(io,:).' ) / Nmc;

  hphio  = hphi (io);
  hatK   = hphi2 - hphi.*hphio.';

  # GP
  nTo    = length (io);
  tmp    = (hatK(io,:) + sy.^2 * eye(nTo))\ [yo hphio];
  alpha  = tmp(:,1);
  halpha = tmp(:,2);
  n      = roots ([halpha.'*hphio nTo -yo.'*alpha]);
  n      = n(n>0);

  gp     =  hatK * (alpha - n * halpha) + n * hphi;
endfunction

tic
[n_gp hphi y_gp] = gp_estim (phi, t, io, yo, ...
                   @(n)lognrnd (a_lpar{:},1,n), @(n)lognrnd (b_lpar{:},1,n), sy);
toc
##
# Least squares estimation of number of delays, obtained from
#
# $$ N_{LS} = \operatorname{argmin} \sum_{i=1}^{n} \left( y_i - N\hat{\varphi}(t)\right)^2 $$
#
n_ls = yo.' * hphi(io) ./ sumsq (hphi(io));
y_ls = n_ls * hphi;


function plot_result (fig, t, to, y, yo,y_ls,y_gp,nd,n_ls,n_gp)
  res = y - [y_ls y_gp];
  err = mean (res.^2);
  printf ("** MSE Error **\nLS:%.2g\t%d/%d\nGP:%.2g\t%d/%d\n", ...
          err(1), round (n_ls), nd, ...
          err(2), round (n_gp), nd);
  fflush (stdout);
  figure (fig)
  clf
  h = plot (t, [y y_ls y_gp], 'linewidth', 2);

  hold on
  h(end+1) = plot (to, yo, 'og', 'markerfacecolor', 'g');
  set (gca, 'color', 'none');
  hold off

  axis tight
  hl = legend (h, {'True', ...
              sprintf("LS Fit (n=%.1f)", n_ls), ...
              sprintf("GP Fit (n=%.1f)", n_gp), ...
              'Observed', ...
              }
         );
  set(hl, 'color', 'none');

endfunction

fig = figure (2)
plot_result (fig, t, to, y, yo,y_ls,y_gp,N,n_ls,n_gp)
