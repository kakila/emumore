#rande ('state', double('abracadabra'));
#randn ('state', rande('state'));
n = 25;
x = cumsum (exprnd (1/(n-1),n,1));
x = (x - min (x)) / (max(x) - min(x));

y_ =@(x) (x - 0.05) .* (x - 0.23) .* (x - 0.95);
y  = y_(x);
sd = (max(y) - min(y)) * 0.1;
y  += sd * randn (n,1);

N = 4;
x0  = linspace (0,1,N); dx = x0(2)- x0(1);
s = N * dx^2;
Phi =@(t) exp (- (t - x0).^2 / s / 2);
Kg  =@(z) [Phi(z) ones(length(z),1)];
kg  = Kg(x);
bg  = kg \ y;

#N  = 60;
#x0  = linspace (0,1,N); dx = x0(2)- x0(1);
#s = 15 * dx^2;
#Phi =@(t) exp (- (t - x0).^2 / s / 2);
#Kg  =@(z) [Phi(z) ones(length(z),1)];
#kg  = Kg(x); kgT = kg.'; kgR = (kgT*kg + 3e-1*eye(N+1));
#bg  = kgR \ (kgT * y);

Kp  =@(z) [z.^3 z.^2 z ones(size(z))];
kp  = Kp(x); %kpT = kp.'; kpR = (kpT*kp + 1e-6*sd^2*eye(4));
bp  = kp \ y;

t  = linspace (-0.2,1.1,200).';
yg = Kg(t) * bg;
yp = Kp(t) * bp;

h = plot (x,y,'o', t, [yg yp y_(t)], '-');
axis tight
grid on
xlabel ('input')
ylabel ('output')
Ptxt = sprintf ('P%d', length (bp));
Gtxt = sprintf ('SE(%d)', length (bg));
legend (h, {'Obs',Gtxt,Ptxt,'Noiseless'},'location','NorthWest');

err = sqrt (mean ((y_(t) - [yg yp]).^2));
printf ('Noise  %g\nErrors:\n%s\t%g\n%s\t%g\n',sd, Gtxt, err(1), Ptxt, err(2));
