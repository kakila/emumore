# Copyright (C) 2017 - Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## Subdomain stability of principal components
# This desiderata of PC encodes some assumptions about the underlying process
# generating the data.
# Here we show a counter example, where subdomain stability is not expected
pkg load gpml
pkg load statistics

nT = 2 * 100;
t  = linspace (0, 3, nT).';

sigma   = @(x) 1 ./ (1 + exp (- (x - mean (t)) / 0.05));
CPK =@(hyp, x, z) sigma (x.') .* covSEiso (hyp(3:4), x, z) .* sigma (z) + ...
                  (1 - sigma (x.') ) .* covSEiso (hyp(1:2), x, z) .* (1 - sigma (z));

## Signal generation
# We generate switching signals defined by covarainces with different length scales

hyp = log([0.08 1 0.2 1]);
K   = CPK(hyp, t, t) + diag (sqrt (eps (1,nT) ) );
Y   = mvnrnd (0, K, 20).';

##
# Plot signals and switching point
figure (1)
clf
plot (t, Y);
hold on
Ypp = max (Y(:)) - min (Y(:));
plot (t, sigma (t) * Ypp + min (Y(:)), '-k', 'linewidth', 2);
axis tight
xlabel ('Time')
ylabel ('Signal')

## Extract PC
# By keeping a fix tolerance we expect a higher numer of components in the
# with faster variations.
# The most important components in the global domain and the sudomians could
# be similar, specially the ones in the domains with smoother functions.
# However many times they are not.

tol = 2e-1;
[V l w] = pod (Y - mean(Y), tol);

idx{1} = 1:nT/2;
idx{2} = nT/2 + (idx{1});
i0     = idx{1}(end);

[V1 l1 w1] = pod (Y(idx{1},:) - mean(Y(idx{1},:)), tol);
[V2 l2 w2] = pod (Y(idx{2},:) - mean(Y(idx{2},:)), tol);

##
# Plot components
fmt = {'horizontalalignment', 'center'};
figure (2)
clf
subplot(2,1,1)
plot (t, V)
axis tight
v = axis ();
line (t(i0)*[1 1], v(3:4));
text (t(i0), v(4)*1.1, sprintf("N=%d", size(V,2)), fmt{:});

subplot(2,1,2)
plot (t(idx{1}), V1, t(idx{2}), V2)
axis tight
v = axis ();
line (t(i0)*[1 1], v(3:4));
text (t(i0)/2, v(4)*1.1, sprintf("N=%d", size(V1,2)), fmt{:});
text (1.5*t(i0), v(4)*1.1, sprintf("N=%d", size(V2,2)), fmt{:});

##
# Plot 1st component scaled to match
figure (3)
c   = corr (V(idx{1},1), V1(:,1));
v11 = V1(:,1) * sign (c);%/ sqrt(l1(1));
c   = corr (V(idx{2},1), V2(:,1));
v21 = V2(:,1) * sign (c);% / sqrt(l2(1));

Vm  = min (V(idx{1},1));
Vpp = max (V(idx{1},1)) - Vm;
v11 = (v11 - min(v11)) / (max(v11) - min (v11)) * Vpp + Vm;
Vm  = min (V(idx{2},1));
Vpp = max (V(idx{2},1)) - Vm;
v21 = (v21 - min(v21)) / (max(v21) - min (v21)) * Vpp + Vm;

clf
h = plot (t, V(:,1), '-k;global;', t(idx{1}), v11, '-r; 1st subd.;', ...
                                   t(idx{2}), v21, '-r; 2nd subd.;');
set (h, 'linewidth', 2);
axis tight
v = axis ();
line (t(i0)*[1 1], v(3:4));
title ('First component')
