## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-05-02

clear all

## Definition of integrand in functional $h_k^s$
#
function v = h_integrand (s, p, t)
  # derivate the polynomial |s| times
  for i = 1:abs(s)
      p = polyder (p);
  endfor

  # evaluate the polynomial
  # sign of the result is given by s/|s|
  p = polyval (p, t) * sign (s);

  v = p .* (1 - sign (p));
endfunction

## Definition of functional
#
function v = h (s, Omega, p)
  func = @(t) h_integrand (s, p, t);
  # The integral could be a non-adaptive quadrature
  v    = quadgk (func, Omega(1), Omega(2));
endfunction

## Generate data for the example
#
n = 100;
t = linspace (-1, 1, n).';
y = tanh (2 * t) + 0.2 * randn (n, 1);

## Define the optimization problem and solve
#

# The parameter vector is composed of D+1 elements describing a polynomial of degree D
# followed by 1 element describing the position of the inflection point
D  = 10; % polynomial degree
O  = D + 1;
p0 = randn (O + 1, 1);
p0(end) = min (max (p0(end), -1), 1); % put guess inside domain

# cost function
cost =@(x) sumsq (polyval (x(1:O), t) - y);
# inequalities
ineq =@(x) [ h(2, [-1 x(end)], x(1:O)); ... % concave 1st half
             h(-2, [x(end) 1], x(1:O))];    % convex  2nd half

# Solve using SQP, I believe that with a fixed quadrature this becomes QP
tic
[p, obj, infocode] = sqp (p0, cost, [], ineq);
toc
t_inf = p(end);   % inflection point
p     = p(1:O); % polynomial coefficients

# Plot solution
hp = plot (t, y, 'x', ...
           t, polyval (p, t), '-', ...
           t_inf, polyval (p, t_inf), 'og');
set (hp(end), 'markerfacecolor', 'auto');
axis tight
