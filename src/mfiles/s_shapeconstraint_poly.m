## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-06-08

## Script testing shape constraints for polynomial regressors
#
clear all

## The optimization problem
# Given a domain $\Omega \in \mathbb{R}^d$, define a finite partition with 
# $K$ subdomains:
#
# $$ P_K(\Omega) = \left\lbrace\Omega_k : \Omega_k \cap\Omega_{k'} = \emptyset, k\neq k' \wedge {\scriptsize\bigcup}_k \Omega_k = \Omega\right\rbrace $$
#
# Find $p(x) \in \Pi(\Omega)$ that minimizes
#
# $$\operatorname{Loss}\left(\lbrace y_i\rbrace, p[\lbrace x_i\rbrace]\right)$$
#
# subject to the functional inequality constraints
#
# $$ h_k^s(\Omega_k, p) := \int_{\Omega_k}\left( p^{(s)}(x) - \vert p^{(s)}(x) \vert \right) dx \geq 0 $$
#

## 1D example
# Definition of integrand in functional $h_k^s$
#
function v = h_integrand (s, p, t)
  # derivate the polynomial |s| times
  for i = 1:abs(s)
    p = polyder (p);
  endfor
  # evaluate the polynomial
  p = polyval (p, t) * sign (s);
  # integrand
  v = p - abs (p); 
endfunction

##
# Definition of functional
#
function v = h (s, Omega, p)
    func = @(t) h_integrand (s, p, t);
    # The integral could be a non-adaptive quadrature
    v    = quadgk (func, Omega(1), Omega(2));
endfunction

## Data
# Generate noisy data on which we want to find an inflection point
n = 100;
t = linspace (-1, 1, n).';
y = tanh (2 * t) + 0.1 * randn (n, 1);

##
# The parameter vector is composed of D+1 elements describing a polynomial of 
# degree D followed by 1 element describing the position of the inflection point.
#
D  = 10; % polynomial degree
O  = D + 1;
p0 = polyfit (t, y, D);
p0(end+1) = 2 * rand - 1; % guess of inflection point
p0 = p0.';
## Cost function
# Data for the gradient of cost
Vd = vander (t, O);
K = Vd.' * Vd;
Vy = Vd.' * y;
H = blkdiag (K, 1);
cost ={@(x) x(1:O).'* ( K * x(1:O) - 2 * Vy );
      @(x) [2*(K*x(1:O)-Vy); 0];
      @(x) H};
# inequalities
ineq =@(x) [ h(2, [-1 x(end)], x(1:O)); ... % concave 1st half
             h(-2, [x(end) 1], x(1:O))];    % convex  2nd half

# TODO: we can use equality conditions to restrict the number of inflection points

# Solve using SQP, I believe that with a fixed quadrature this becomes QP
tic
[p, obj, infocode] = sqp (p0, cost, [], ineq);
toc
t_inf = p(end); % inflection point
p     = p(1:O); % polynomial coefficients

# Plot solution
hp = plot (t, y, 'x', ...
           t, polyval (p, t), '-', ...
           t_inf, polyval (p, t_inf), 'og');
set (hp(end), 'markerfacecolor', 'auto');
line (0,[min(y) max(y)], 'linestyle', '--');
axis tight

Nk = 10;
if yes_or_no (sprintf ('Do you want to run %d solutions? ', Nk));
  elapsed = t_inf = zeros (Nk, 1);
  tic
  for k=1:Nk
    y  = tanh (2 * t) + 0.1 * randn (n, 1);
    p0 = polyfit (t, y, O);

    elapsed(k) = cputime ();
    [p, obj, infocode] = sqp (p0, cost, [], ineq);
    elapsed(k) = cputime () - elapsed(k);

    t_inf(k) = p(end);   % inflection point
  endfor
  toc
endif

