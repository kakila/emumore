## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## Script to test SVD (POD) reduction on general linear problems

pkg load geometry
# Define parametrized problem
function [A b p t_] = sysMat (x,N=1)
  persistent p t C;
  if size(p,1) != N
    fpolar = @(a)[cos(pi*a) sin(pi*a)];
    p = fpolar (linspace (0,1,N).');
    disp ("Setting center(s)");
    #disp (p);
    t = 0.5 * fpolar (linspace (0,2-1/N,N).');
    disp ("Setting target(s)");
    #disp (t);
    C = sprandsym (N*2, 0.5) .* (triu (ones (N*2),2) - tril (ones (N*2),-2));
  endif
  t_ = t;# + x(2:3);

  tmp = tmpi = tmb = cell (1,N);
  for i = 1:N
    T      = createHomothecy (p(i,:), x(1));
    tmp{i} = sparse (T(1:2,1:2));
    tmb{i} = t_(i,:).' - T(1:2,3);
  endfor
  A = blkdiag (tmp{:}) + 1e-2 * C;
  b = vertcat (tmb{:});
endfunction

## Generate data
N         = 50;
Nx_trn    = 40; # Training data
Nx_tst    = 1;  # Test data
x         = zeros (Nx_trn+Nx_tst, 3);
x(:,1)    = logspace (-0.2, 1, Nx_trn+Nx_tst);
x(:,2)    = linspace (-1e0, 1e0, Nx_trn+Nx_tst);
x(:,3)    = linspace (-5e-1, 5e-1, Nx_trn+Nx_tst);
x(:,2)    = x(randperm(Nx_trn+Nx_tst),2);
x(:,3)    = x(randperm(Nx_trn+Nx_tst),3);
idx_trn   = sort (randperm (Nx_trn+Nx_tst,Nx_trn));
idx_tst   = setdiff (1:Nx_trn+Nx_tst, idx_trn);

u  = zeros (N*2, Nx_trn);
pu = pt = pt_ = cell (Nx_trn, 1);
for i = 1:Nx_trn
  [A b c t] = sysMat (x(idx_trn(i),:), N);
  du        = t.'(:) - b;

  u(:,i) = A \ b;
  pu{i}  = reshape (u(:,i), [2,N]).';
  pt_{i} = reshape (A * u(:,i) + du, [2,N]).'; # polygon of recovered targets
  pt{i}  = t;                                  # polygon of targets
endfor

figure (1, 'color', 0.9 * ones (1,3));
clf;
h = drawPolygon (pt_);
set (h, 'color', 'k', 'linestyle', '--');
hold on
drawPoint (c, '*k');
drawCircleArc (0, 0, 1, 0, 180);
#drawPoint (cell2mat(pt_), 'og'); # Verify solution
#for i=1:N
#  drawEdge (c(i,1),c(i,2), t(i,1),t(i,2),'color','k','linestyle','--');
#endfor
h   = drawPolygon (pu);
col = spring (Nx_trn);
arrayfun (@(n)set(h(n),'color',col(n,:)), 1:Nx_trn);
hold off
axis image

input ('Showing traning data ... <press key to continue>');

## Do SVD or POD
[P S W] = svd (u, 1);
# Compute the error as we consider increasing number of components
s    = diag (S);
nerr = sqrt (1 - cumsum (s.^2) ./ sumsq (s));
# number of nodes
tol = 1e-3;  # Desired relative tolerance in Frobenius norm
ns  = find (nerr < tol, 1, 'first');
V   = P(:, 1:ns);
VT  = V.';
### Estimate
u_n  = zeros (ns, Nx_tst);
pu_n = pu = pt = cell (Nx_tst, 1);
err  = 0;
t_ref = t_n = 0;
for i = 1:Nx_tst
  [A b c t]  = sysMat (x(idx_tst(i),:), N);
  pt{i}      = t; # polygon of targets
  tic;
  u(:,i) = A \ b; # reference value
  t_ref += toc;

  pu{i}  = reshape (u(:,i), [2,N]).';

  tic
  A_n    = VT * A * V;
  b_n    = VT * b;
  u_n(:,i) = A_n \ b_n;
  t_n += toc;

  tmp      = V * u_n(:,i);
  pu_n{i}  = reshape (tmp, [2,N]).'; # estimated polygon
  err      = max (err, max (abs (u(:,i)-tmp)));
endfor
fprintf ("Test data in green.\nMax error: %.2g\n", err)
fprintf ("Reference time: %.2g\n", t_ref)
fprintf ("Reduced time: %.2g\n", t_n)
fprintf ("speed factor: %.2g\n", t_ref / t_n);

figure (1)
hold on
h = drawPolygon (pt);
col = [0 1 0];
set (h, 'color', 0.7*col, 'linestyle', '-');
h = drawPolygon (pu_n);
set (h, 'color', 0.7*col);
h = drawPoint (cell2mat(pu),'o');
set (h, 'color', 0.5*col, 'markerfacecolor', 0.5*col);
axis image
hold off
