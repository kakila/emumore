## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## Levels of two connected tanks when surface velocities are not negligible
## Equations obetained using unsteady Bernoulli, i.e. along a streamline
##
## P1/rho + g h1 + 0.5 (dh1/dt)^2 = P2/rho + g h2 + 0.5 (dh2/dt)^2 + integreal (dv(t,s)/dt, s, 1, 2)
##
## where v is the velocity along the streamline s.
##
## The integral was split in three segments, in tank 1, in tank 2 and in the
## connecting pipe of length L. The variation of v as a function of time in the
## two tanks is neglected, hence only one term survives.
##
## The resulting ode is
## dh1/dt = -A/A1 v
## dh2/dt =  A/A2 v
## dv/dt  = g/L * (h1 - h2) + [ (dh1/dt)^2 - (dh2/dt)^2 ]/2/L
##
## With th econvetion that positive v means flow form tank 1 to tank 2.
## The last equation can be written in terms of v
## dv/dt  = g/L * (h1 - h2) + A^2 * [1/A1^2 - 1/A2^2]/2/L * v^2
## dv/dt  = g/L * (h1 - h2) + A^2 * (A1^2 - A2^2)/(A1*A2)/2/L * v^2
##
## In the case of A1 == A2
## dh1/dt = -A/A1 v
## dh2/dt =  A/A2 v
## dv/dt  = g/L * (h1 - h2)
##
## In matrix form
## dx/dt = A x
##      0    0  -A/A1
## A =  0    0   A/A2
##     g/L -g/L   0

# cross-section Area of tanks
opt.A1 = 1;
opt.A2 = 100;
# geometry of connecting pipe
opt.L  = 0.001;
opt.A  = 0.02 * min(opt.A1, opt.A2);  # by assumption this should be small(?)
# gravity
opt.g = 9.81;

function dxdt = twotanks (t,x,opt)
  dxdt      = zeros (3, length (t));
  dxdt(1,:) = - opt.A / opt.A1 * x(3,:); # dh1/dt
  dxdt(2,:) =   opt.A / opt.A2 * x(3,:); # dh2/dt

  K = opt.A^2 * (opt.A1^2 - opt.A2^2) / (opt.A1*opt.A2) / 2 / opt.L;
  dxdt(3,:) = opt.g/opt.L * ( x(1,:) - x(2,:) ) + K * x(3,:);

  dxdt(1,(x(1,:)<=eps) & (x(3,:) > 0)) = 0;
  dxdt(2,(x(2,:)<=eps) & (x(3,:) < 0)) = 0;
endfunction

x0 = [1 0 sqrt(2*opt.g)];
f  = sqrt ((1/opt.A1 + 1/opt.A2)*opt.A*opt.g/opt.L) / 2 / pi;
T  = 1/f;
[t,x] = ode45 (@(t,x)twotanks(t,x,opt), [0 10*T], x0);

plot (t,x(:,1:2))
axis tight
