## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defun {@var{} =} meanSuperpos (@var{}, @var{})
## 
## @seealso{}
## @end defun

function m = meanSuperpos (t, dlim, slim)
  # Mapping to unit square
  [td ts] = unitsquare (t, dlim, slim);
  

endfunction

function [td ts] = unitsquare (t, d, s)
  td = ( t - d(1) ) / (d(2) - d(1));
  ts = ( t - s(1) ) / (s(2) - s(1));
endfunction
