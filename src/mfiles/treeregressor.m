## Copyright (C) 2017 - Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [y_,yl,yu,idx,ranks,dev] = treeregressor (x, y, xtst=[], varargin)

  if nargin > 3
    args = varargin;
  endif

  # Write inputs
  xfile = '/tmp/skl_X.mat';
  save ('-v7', xfile, 'x');
  yfile = '/tmp/skl_Y.mat';
  save ('-v7', yfile, 'y');
  if xtst
    tstfile = '/tmp/skl_Xtst.mat';
    save ('-v7', tstfile, 'xtst');
  endif
  
  # Write params
  pfile  = '/tmp/skl_P.mat';
  astype = @(v,x,t)sprintf(sprintf("%s = %s;", v, t), x);
  fmt = {"%d", "%g", "'%s'"};
  for i = 1:2:numel(args)
    if ischar(args{i+1})
      dtype = 3;
    else
      if args{i+1} - fix (args{i+1}) < eps
        dtype = 1;
        args{i+1} = int32 (args{i+1});
      else
        dtype = 2;
      endif
    endif
    eval ( astype (args{i}, args{i+1}, fmt{dtype}));
  endfor
  save('-v7', pfile, args{1:2:end});

  [basep, fname] = fileparts (mfilename ("fullpath"));
  func  = fullfile (basep,'..','pyfiles','skl_treeregressor.py');
  if xtst
    cmd = sprintf ("python3 %s -x %s -y %s -t %s -p %s", func, xfile, ...
       yfile, tstfile, pfile);
  else
    cmd = sprintf ("python3 %s -x %s -y %s -p %s", func, xfile, yfile, pfile);
  endif

  [s o] = system (cmd);
  if s
    disp (o)
    error('Octave:runtime-error', 'python call failed');
  endif
  resfile = '/tmp/skl_TreeRegressor_results.mat';
  load (resfile);

  if xtst
    y_ = struct('trn',y_,'tst', ytst);
    yu = struct('trn',yu,'tst', ytstu);
    yl = struct('trn',yl,'tst', ytstl);
  endif
  
  if exist ('oob_R2','var')
    if isstruct(y_)
      y_.R2 = oob_R2;
    else
      y_ = struct('trn',y_,'R2', oob_R2);
    endif
  endif
  
%  delete (xfile, yfile, pfile, resfile);
endfunction

%!demo
%! x = randn (100, 5);
%! y = tanh (mean(x(:,[1 3 5]),2)) - sin(2*pi*x(:,2)) + 0.05 * randn (100,1);
%! method = {'GradientBoostingRegressor', 'ExtraTreesRegressor'};
%!
%! for im = 1:numel(method)
%!   m = method{im};
%!   switch m
%!     case 'GradientBoostingRegressor'
%!       args = {'max_leaf_nodes',8, 'learning_rate',.1};
%!     case 'ExtraTreesRegressor'
%!       args = {'n_estimators',200, 'bootstrap', true, 'n_jobs', nproc-1};
%!   endswitch
%!   [y_,yl,yu,idx,r,s] = treeregressor (x,y, 'method', m, args{:});
%!
%!   figure (2*im-1)
%!   [yo o] = sort (y, 'ascend');
%!   pp = [yo yl(o);flipud(yo) flipud(yu(o))];
%!   patch (pp(:,1),pp(:,2), 0.9*ones(1,3), 'edgecolor', 'none')
%!   hold on
%!   h = plot (y,y_,'or', [min(y) max(y)], [min(y) max(y)],'-k');
%!   set (h(1),'markerfacecolor','r');
%!   hold off
%!   axis tight
%!   grid on
%!   xlabel ('y');
%!   ylabel ('predicted y');
%!   title (m);
%!
%!   figure (2*im)
%!   bar (1:5, r, 'r');
%!   hold on
%!   line ([1:5;1:5],r+[-s; s])
%!   axis tight
%!   grid on
%!   xlabel ('feature');
%!   ylabel ('importance');
%!   title (m);
%! endfor

%!demo
%! x = randn (100, 1);
%! y = sin(2*pi*x) + 0.05 * randn (100,1);
%! xtst = linspace (min(x)*1.2, max(x)*1.2, 200).';
%! ytst = sin(2*pi*xtst);
%! method = {'GradientBoostingRegressor', 'ExtraTreesRegressor'};
%!
%! for im = 1:numel(method)
%!   m = method{im};
%!   switch m
%!     case 'GradientBoostingRegressor'
%!       args = {'n_estimators',200, 'learning_rate',.1};
%!     case 'ExtraTreesRegressor'
%!       args = {'n_estimators',200, 'bootstrap', false, 'n_jobs', nproc-1, ...
%!              'oob_score', false};
%!   endswitch
%!   [y_,yl,yu,idx,r,s] = treeregressor (x,y, xtst, 'method', m, args{:});
%!   figure (im)
%!   pp = [xtst yl.tst;flipud(xtst) flipud(yu.tst)];
%!   patch (pp(:,1),pp(:,2), 0.9*ones(1,3), 'edgecolor', 'none')
%!   hold on
%!   h = plot (x,y,'or', xtst,y_.tst,'-k', xtst, ytst,'--');
%!   set (h(1),'markerfacecolor','auto');
%!   hold off
%!   axis tight
%!   grid on
%!   xlabel ('y');
%!   ylabel ('x');
%!   if isfield(y_,'R2')
%!     title (sprintf('%s, R²:%.2f',m,y_.R2))
%!   else
%!     title (m);
%!   endif
%! endfor
