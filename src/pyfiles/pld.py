'''
 Copyright (C) 2017 - Juan Pablo Carbajal

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
  
def all_leafs (T, source=0):
  l = [n for n,d in T.degree_iter() if d == 1 and n != source]
  return np.sort(l)

def all_paths (T, source=0):
  leafs = all_leafs(T, source=source)
  return tuple([nx.shortest_path(T,source,n) for n in leafs]), tuple(leafs)

def lambda_iter (T,p):
  return (\
    e[2]['l'] for i,e in enumerate(T.edges_iter(p, data=True)) if e[0] in p and e[1] in p \
    )

def path_factorial (T, path):
  l1,l2,l3 = [],[],[1]
  for x,y,z in lambda_iter(T, path):
    l1.append(x)
    l2.append(y)
    l3.append(z)
  
  tmp   = np.cumprod (l3)
  pfact = np.zeros(3,)
  pfact[0] = np.prod(l1)
  pfact[2] = tmp[-1]

  pfact[1] = np.sum(np.multiply (l2, tmp[0:-1]))

  return pfact

def populate_tree (T, l):
  for x,e in zip(l, T.edges_iter()):
    T[e[0]][e[1]]['l'] = x

def flatten_tree (T):
  P,L = all_paths(T)
  pFact = [(l,path_factorial(T,p)) for p,l in zip(P,L)]
  
  Tf = nx.Graph()
  for v in pFact:
    Tf.add_edge(0,v[0],l=v[1])
  
  return Tf
  
def plot_tree (T, title=None):
  from networkx.drawing.nx_agraph import graphviz_layout

  # same layout using matplotlib with no labels
  plt.figure()
  if title:
    plt.title(title)
  plt.subplot(2,1,1)
  pos = graphviz_layout(T, prog='dot')
  nx.draw(T, pos, with_labels=True, arrows=False)

  Tf = flatten_tree(T)
  posf = graphviz_layout(Tf, prog='dot')
  xM = np.max([v[0] for v in pos.values()])
  xm = np.min([v[0] for v in pos.values()])
  dx = xM - xm
  for k,v in posf.items():
    posf[k] = (v[0]+xM+0.1*dx,v[1])
  nx.draw(Tf, posf, with_labels=True, arrows=False)

  plt.subplot(2,1,2)
  data = np.array([list(v[2]['l']) for v in Tf.edges_iter(data=True)])
  h = plt.hist(data, label=('factor', 'delay','scale'))
  plt.legend()
  plt.show()

if __name__ == "__main__":
  plt.close('all')
  plt.ion()
  T = nx.Graph()
  T.add_edges_from ([(0,1), (0,2), (2,3), (2,4)])
  l = [(0.9,1,2),(1,2,1.1),(0.5,3,1),(0.5,3,2)]
  populate_tree (T, l)
  
  P,L = all_paths(T)
  pFact = [(l,path_factorial(T,p)) for p,l in zip(P,L)]
  title = 'Simple Tree'
  print (title)
  for l,f in pFact:
    print('Leaf:{0:d}, factor:{1:.2f}, delay:{2:.2f}, scale:{3:.2f}'.format(l,*f))

  plot_tree(T,title)
  
  epdf = np.random.exponential
  lpdf = lambda n: [(*x[0].tolist(),*x[1].tolist(),*x[2].tolist()) for x in zip(np.minimum(0.8+epdf(1.0,[n,1]),1.0),epdf(1.0,[n,1]),epdf(1.5,[n,1]))]

  T = nx.balanced_tree(2,4)
  Ne = T.number_of_edges()
  Nn = T.number_of_nodes()

  l =  lpdf(Ne)
  populate_tree (T, l)
  P,L = all_paths(T)

  pFact = [(l,path_factorial(T,p)) for p,l in zip(P,L)]
  title = 'Balanced Tree'
  print (title)
  for l,f in pFact:
    print('Leaf:{0:d}, factor:{1:.2f}, delay:{2:.2f}, scale:{3:.2f}'.format(l,*f))

  plot_tree(T,title)

  T = nx.Graph()
  T.add_node(0)
  while T.number_of_nodes() < Nn:
    n = T.nodes()[-1]
    l = all_leafs(T)
    if len(l) > 0:
      r = l[np.random.randint(0,len(l))]
    else:
      r = 0
    T.add_edges_from( [ (r,n+y+1) for y in range(np.random.randint(2,5)) ] )
    
  Ne = T.number_of_edges()

  l =  lpdf(Ne)
  populate_tree (T, l)
  P,L = all_paths(T)

  pFact = [(l,path_factorial(T,p)) for p,l in zip(P,L)]
  title = 'Random Tree'
  print (title)
  for l,f in pFact:
    print('Leaf:{0:d}, factor:{1:.2f}, delay:{2:.2f}, scale:{3:.2f}'.format(l,*f))

  plot_tree(T,title)

  plt.ioff()
