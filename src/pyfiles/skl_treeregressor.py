'''
 Copyright (C) 2017 - Juan Pablo Carbajal

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>


import numpy as np
import sklearn.ensemble as ske

ARGS={'GradientBoostingRegressor': \
        set(['loss',\
             'learning_rate',\
             'n_estimators',\
             'subsample',\
             'criterion',\
             'min_samples_split',\
             'min_samples_leaf',\
             'min_weight_fraction_leaf',\
             'max_depth',\
             'min_impurity_decrease',\
             'min_impurity_split',\
             'init',\
             'random_state',\
             'max_features',\
             'alpha',\
             'verbose',\
             'max_leaf_nodes',\
             'warm_start',\
             'presort']), \
      'ExtraTreesRegressor': \
        set(['n_estimators', \
             'criterion', \
             'max_depth', \
             'min_samples_split', \
             'min_samples_leaf', \
             'min_weight_fraction_leaf', \
             'max_features', \
             'max_leaf_nodes', \
             'min_impurity_decrease', \
             'min_impurity_split', \
             'bootstrap', \
             'oob_score', \
             'n_jobs', \
             'random_state', \
             'verbose', \
             'warm_start'])
     }
def skl_TreeRegressor (x, y, method=None, xtst=[], opt={}):
  
  n_samples, n_features = x.shape
  
  x = x.astype(np.float32)
  y = y.astype(np.float32)

  do_test = False
  if len(xtst) == 0:
    xtst = np.array([])
  else:
    do_test = True

  if not method:
    method='GradientBoostingRegressor'

  regressor = getattr(ske, method)

  alpha = 0.95
  if opt:
    if 'alpha' in opt.keys():
      alpha = opt['alpha']
      if alpha < 0.5:
        alpha = 1.0 - alpha

    for k in set(opt.keys())-ARGS[method]:
        opt.pop(k, None)

  emu = regressor(**opt)
  # Best prediction
  emu.fit(x, y)
  y_ =  np.atleast_2d(emu.predict(x)).T
  if do_test:
    ytst = np.atleast_2d(emu.predict(xtst)).T

  ranks = emu.feature_importances_
  if method == 'GradientBoostingRegressor':
    std = np.std([tree[0].feature_importances_ for tree in emu.estimators_], axis=0)
  elif method == 'ExtraTreesRegressor':
    std = np.std([tree.feature_importances_ for tree in emu.estimators_], axis=0)

  idx   = np.argsort(ranks)[::-1]
  
  if method == 'GradientBoostingRegressor':
    # Predict on data with given alpha
    emu.set_params(loss='quantile')
    emu.fit(x, y)
    yu = np.atleast_2d(emu.predict(x)).T
    if do_test:
      ytstu = np.atleast_2d(emu.predict(xtst)).T
    # Predict on data with complementary alpha
    emu.set_params(alpha=1.0 - alpha)
    emu.fit(x, y)
    yl = np.atleast_2d(emu.predict(x)).T
    if do_test:
      ytstl = np.atleast_2d(emu.predict(xtst)).T

  elif method == 'ExtraTreesRegressor':
    err,p = pred_ints(emu, x, alpha)
    yl = np.atleast_2d(err[0,:]).T
    yu = np.atleast_2d(err[1,:]).T
    if do_test:
      err,p = pred_ints(emu, xtst, alpha)
      ytstl = np.atleast_2d(err[0,:]).T
      ytstu = np.atleast_2d(err[1,:]).T

  retval = {'y_':y_, 'yl': yl, 'yu': yu, 'idx': idx, 'ranks': ranks, 'dev': std, 'reg': emu}
  if do_test:
    retval['ytst'] = ytst
    retval['ytstu'] = ytstu
    retval['ytstl'] = ytstl
  if opt.pop('oob_score', False):
    retval['oob_R2'] = emu.oob_score_
  if method == 'ExtraTreesRegressor':
    retval['preds'] = p

  return retval

def pred_ints(model, X, percentile=0.95):
  pv = [100*(1-percentile),100*percentile]
  preds = []
  for est in model.estimators_:
    try:
      tmp = est.predict(X)
    except AttributeError:
      tmp = est[0].predict(X)
    preds.append(tmp)

  preds = np.atleast_2d(preds)
  err = np.percentile(preds, pv, axis=0)

  return err,preds

if __name__ == "__main__":
  import getopt
  def parse_options (args):
    hlp_str = 'skl_treeregressor.py -x <input data> -y <output data> -p <param file> -t <test inputs> -v <verbose>'
    options = {'x':'/tmp/skl_X.mat', 'y':'/tmp/skl_Y.mat', \
      'p':'/tmp/skl_P.mat', 't':'', 'v':False}
    try:
      opts, args = getopt.getopt(args,"hx:y:p:t:v:")
    except getopt.GetoptError:
      print (hlp_str)
      sys.exit(2)
    for opt, arg in opts:
      if opt == '-h':
        print (hlp_str)
        sys.exit()
      elif opt in ("-x","-y","-p","-t","-v"):
        options[opt[1]] = arg
    return options

  import sys
  opts = parse_options(sys.argv[1:])
  verbose = opts.pop('v', False)
  if verbose:
    for v in ("x","y"):
      print ("Read {} from {}".format(v, opts[v]))
    print ("Read paramters from {}".format(opts['p']))

  from scipy.io import loadmat, savemat
  X = loadmat(opts['x'])['x']
  Y = loadmat(opts['y'])['y']
  Xtst = np.array([])
  if opts['t']:
    if verbose:
      print ("Read test inputs from {}".format(opts['t']))
    Xtst = loadmat(opts['t'])['xtst']
    
  n_samples, n_outputs = Y.shape
  if n_outputs == 1:
    Y = Y.reshape (n_samples, )
  tmp = loadmat(opts['p'])
  P = {v:k.flatten()[0] for v,k in tmp.items() if v[0] is not "_"}
  for k,v in P.items():
    if not isinstance (v, np.str_):
      if v - np.fix(v) == 0:
        P[k] = int (v)
  method = P.pop('method', None)
  result = skl_TreeRegressor (X, Y, method, Xtst, P)
  emu    = result.pop('reg')
  preds  = result.pop('preds', None)
  savemat ('/tmp/skl_TreeRegressor_results', result)

