'''
 Copyright (C) 2017 - Juan Pablo Carbajal

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>


from sage.all import *
import re

def operandize (o):
  """
    Returns operands of expresion o recursively.

    Sage symbols are returned as strings

    Example:

      ::

        $ var('L T M')
        $ expr = M * L / T^2
        $ operandize (expr)
        ['L', 'M', ['T', -2]]

  """

  if o.is_symbol():
    return str (o)
  elif o.is_constant():
    return o
  else:
    return [operandize(oo) for oo in o.operands()]

def parse_units (units):
  """
    Returns a dictionary with unique units names as keys()
    and their exponents as values.

    The input can be a list, in which case the exponents are rows
    each one correspoding to an element in the input

    The strings defining units expressions are interpreted as sagemath
    expressions, but the behavior is only defined for operands *, ^, and /

    Examples:

      ::

        $ units = 'L^2/T'
        $ parse_units (units)
        {'L': [2], 'T': [-1]}

      ::

        $ units = ['L^2/T','L/T^2','M * L^(2/3) / T^(1/2)']
        $ parse_units (units)
        {'L': [2, 1, 2/3], 'M': [0, 0, 1], 'T': [-1, -2, -1/2]}

  """

  if type(units) is not list:
    units = [units]

  # Find unique units names
  u = [re.findall(r'[a-zA-Z]+',u) for u in units]
  u_units = set([unit for expr in u for unit in expr])
  # Create variables with those names
  var (*u_units)

  # Convert the units to sage expressions
  # and extract exponents
  retval = {k: [] for k in u_units}

  for u in units:
    expr = sage_eval (u, locals=globals())
    expr = operandize (expr)

    # check for sublists
    not_present = u_units.copy()
    if type(expr) is list:
      if any([type(x) is list for x in expr]):
        for v in expr:
          if type(v) is list:
            retval[v[0]].append(v[1])
            not_present.remove(v[0])
          else:
            retval[v].append(1)
            not_present.remove(v)
      else:
        retval[expr[0]].append(expr[1])
        not_present.remove(expr[0])

    else:
      retval[expr].append(1)
      not_present.remove(expr)

    for x in not_present:
      retval[x].append(0)

  return retval

def dict2matrix(d):
  """ Converts a dictionary of variable names and units into a rational matrix.
      The meaning of the rows and colums of the matrix is given by the second
      and third output values

      Returns:
        M (matrix): The matrix with exponents.

        v (tuple):  Ordered names of variables (key of input).

        u (tuple):  Ordered unit names (extracted from values of input).

    Example:

      ::

        $ units = {'dAdt':'L^2/T','v':'L/T','a':'L/T^2'}
        $ M,v,u = dict2matrix (units)
        $ M
        [ 1  2  1]
        [-2 -1 -1]
        $ v
        ('a', 'dAdt', 'v')
        $ u
        ('L', 'T')

  """
  var        = tuple (d.keys())
  units_expr = d.values()

  tmp = parse_units (units_expr)

  units = tuple (tmp.keys())
  rows  = tuple (tmp.values())

  M = matrix (QQ, len(units), len(var))
  for i,r in enumerate(rows):
    M[i,:] = [r]

  return M, var, units

def adimensionalize (M, v):
  """
  Returns adimnesional factors of varibales in v
  built from the exponent matrix M.

  A second output arguments continas the basis of the null-space of M.

    Example:

      ::

        $ units = {'g':'L/T^2','l':'L','m':'M', 'w':'T^(-1)'}
        $ M, v, u = dict2matrix (units)
        $ f, n = adimensionalize(M,v)
        $ f
        ['w^1 * l^1/2 * g^-1/2']

  """
  N = M.right_kernel().basis()
  afactor = []
  for n in N:
    str_factor = []
    for x,y in zip (v,n):
      if y != 0:
        str_factor.append ('^'.join([x,str(y)]))
    afactor.append(' * '.join(str_factor))

  return afactor, N
