=============================
Case study: biocide transport
=============================
:Authors:
    JuanPi Carbajal <ajuanpi@gmail.com>

.. contents:: Table of Contents

Introduction
------------

Growth of micro-organisms on coated substrates is a common problem, since it reduces the performance of materials, in terms of durability as well as aesthetics. In order to prevent microbial growth biocides are frequently added to coatings. Unfortunately, early release of these biocides reduces the biocidal protection of these coatings. Furthermore, since biocides leach to the environment, their release rate is a key parameter determining the environmental impact of biocides [Erich2016]_ .

Mathematical model
------------------
In [Erich2016]_ the following model is provided without derivation

.. math::
   \frac{\partial c_w}{\partial t} & = D_w \frac{\partial^2 c_w}{\partial x^2}\\
   \frac{\partial c_b}{\partial t} & = D_b \left(\frac{\partial^2 c_b}{\partial x^2} - \frac{\partial}{\partial x}\frac{c_b}{c_w}\frac{\partial c_w}{\partial x}\right) + D_w \rho_w \frac{\partial}{\partial x} \frac{c_b}{c_w^2}\frac{\partial c_w}{\partial x} - \frac{\partial c_s}{\partial t}\\
  \frac{\partial c_s}{\partial t} & = k \left( c_b - \frac{c_{\mathrm{sat}}}{\rho_w}c_w\right)

where the subindex :math:`w` indicates water, :math:`b` biocide, and :math:`s` dissolved biocide.

The authors of [Erich2016]_ seem not to have realized that dynamics of the concentration of water (:math:`c_w`) can be solved independently of all the other equations. Similarly they seem not to have noted that the concentration of dissolved biocide (:math:`c_s`) is an ODE that can be integrated after the FEM problem for the biocide (:math:`c_b`) is computed.

Hence the coupled problem is indeed an uncoupled problem of water diffusion. The solution of this problem is feed to the dynamics of the concentration of the biocide, and then we integrate the ODE to know the concentration of dissolved biocide.

Water diffusion problem
+++++++++++++++++++++++
In [Erich2016]_ the water concentration problem is defined with Dirichlet boundary conditions at the end of a domain of length :math:`L` and Neumann at 0.

.. math::
   \left.\frac{\partial c_w(x,t)}{\partial x}\right|_{x=0} &= 0\\
   \left.c_w(x,t)\right|_{x=L} &= c_{wL} f(t)

where :math:`f(t) \in [0,1]` is a waveform that takes different forms in different scenarios, namely constant, and sigmoid type (or linear with threshold).

To avoid solving multiple problems for a wide variety of situations we will adimensionalize the equation. We name :math:`\hat{x},\hat{t},\hat{c}_w` the variables with dimensions and without the decoration the adimnsionalized ones. Then the original equation:

.. math::
   \frac{\partial \hat{c}_w}{\partial \hat{t}} & = D_w \frac{\partial^2 \hat{c}_w}{\partial \hat{x}^2}\\

Can be written using :math:`\hat{x} = L x`, :math:`\hat{t} = T t`, :math:`\hat{c}_w = c_{wL} c_w`, giving

.. math::
   \frac{\partial c_w}{\partial t} =& \frac{D_w T}{L^2} \frac{\partial^2 c_w}{\partial x^2}\\
   \left.\frac{\partial c_w(x,t)}{\partial x}\right|_{x=0} = 0,& \quad
   \left.c_w(x,t)\right|_{x=1} = f(t)

This means that we need to solve a single problem for a huge variety of situations, e.g. different constants in the boundary condition at :math:`x=L`.

The initial condition for the water in the coating is

.. math::
   c_w(x,0) = 0


Dissolved biocide diffusion-advection problem
+++++++++++++++++++++++++++++++++++++++++++++

Since the concentration of biocide :math:`c_b` does not affect the concentration of water, the variable $c_w$ is prescribed. We proceed to expand the derivatives in the equation for :math:`c_b`

.. math::
   \frac{\partial}{\partial x}\frac{c_b}{c_w}\frac{\partial c_w}{\partial x} =& c_b\, \frac{\partial}{\partial x}\frac{\partial \ln c_w}{\partial x} + \frac{\partial c_b}{\partial x}\,\frac{\partial \ln c_w}{\partial x} = c_b\, \left[\frac{1}{c_w}\frac{\partial^2 c_w}{\partial x^2} - \left(\frac{\partial \ln c_w}{\partial x}\right)^2\right] + \frac{\partial c_b}{\partial x}\,\frac{\partial \ln c_w}{\partial x}\\
   \frac{\partial}{\partial x} \frac{c_b}{c_w^2}\frac{\partial c_w}{\partial x} =& c_b\, \left[\frac{1}{c_w}\frac{\partial}{\partial x}\frac{\partial \ln c_w}{\partial x} - \frac{1}{c_w}\left(\frac{\partial \ln c_w}{\partial x}\right)^2\right] + \frac{\partial c_b}{\partial x}\, \frac{1}{c_w}\frac{\partial \ln c_w}{\partial x} = \\
   =& c_b\, \frac{1}{c_w}\left[\frac{1}{c_w}\frac{\partial^2 c_w}{\partial x^2} - 2 \left(\frac{\partial \ln c_w}{\partial x}\right)^2\right] + \frac{\partial c_b}{\partial x}\,\frac{1}{c_w}\frac{\partial \ln c_w}{\partial x}

Multiplying by the respective coefficients and adding together gives

.. math::
   \frac{\partial c_b}{\partial t} =& D_b\, \frac{\partial^2 c_b}{\partial x^2} + \left( D_b + \frac{D_w \rho_w}{c_w}\right)\frac{\partial \ln c_w}{\partial x}\,\frac{\partial c_b}{\partial x} - \\
   -& \left[ \left(D_b + \frac{D_w \rho_w}{c_w}\right)\frac{1}{c_w}\frac{\partial^2 c_w}{\partial x^2} - \left(D_b - 2\frac{D_w \rho_w}{c_w}\right) \left(\frac{\partial \ln c_w}{\partial x}\right)^2 - k\right]\, c_b + \frac{kc_{\mathrm{sat}}c_w}{\rho_w}\\
   =& D_b\, \frac{\partial^2 c_b}{\partial x^2} + A(c_w)\,\frac{\partial c_b}{\partial x} + B(c_w)\, c_b + C(c_w)

where :math:`A(c_w)`, :math:`B(c_w)`, and :math:`C(c_w)` are given functions.

This is an equivalent formula for the dynamics of :math:`c_b` but here the separation between data and variables to integrate is explicit.

The boundary conditions on :math:`c_b(x,t)` are

.. math::
   \left. \frac{\partial c_b(x,t)}{\partial x} \right|_{x=0} &= 0\\
   \left. c_b(x,t) \right|_{x=L} &= 0


The initial condition for the biocide in the coating is

.. math::
   c_w(x,0) = 0

Coating biocide problem
+++++++++++++++++++++++

Once the two problems described above are solved we can integrate the ODE that describes the dynamics of the biocide in the coating :math:`c_s`,

.. math::
   \frac{\partial c_s}{\partial t} = k \left( c_b - \frac{c_{\mathrm{sat}}}{\rho_w}c_w\right)\\

With initial conditions

.. math::
   c_s(x,0) = c_{s0}

In integral form (since :math:`c_b` and :math:`c_w` are given).

.. math::
   c_s(x,t) = c_{s0} + k \int_0^T \left( c_b - \frac{c_{\mathrm{sat}}}{\rho_w}c_w\right) \mathrm{d}\!t

It can also be adimentionalized using :math:`\hat{c}_s = c_{s0} c_s`
This gives

.. math::
   \frac{\partial c_s}{\partial t} &= Tk \left( c_b - \frac{c_{wL} c_{\mathrm{sat}}}{c_{s0}\rho_w}c_w\right)\\
   c_s(x,0) &= 1


References
----------

.. [Erich2016] Erich, S. J. F., & Baukh, V. (2016).
   Modelling biocide release based on coating properties. Progress in Organic Coatings, 90, 171–177.
   http://doi.org/10.1016/j.porgcoat.2015.10.009
