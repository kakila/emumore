## Lists of collaborators ##
:e-mail: means directly contacted.

:white_check_mark: means they are participating in the event.

:newspaper: means we have a dataset for pre-analysis.

:no_entry_sign: means they are out of the loop.

### Eawag
Status | Head name | Contact point | Department | Application (Readiness) | Runtime
-----------------|-----------------|-------------------|-----------------|---------------------------------|----------------
:e-mail: | Martin Schmid | Ulrike Kobler | SURF | Interactions between physics and geochemistry in lakes (REACON 1) | 5-8 hr
:no_entry_sign: | Linda Strande | Nienke Andriessen | SANDEC | Engineering approach for selection and design of faecal sludge treatment technologies (REACON 2) | ?
:white_check_mark: | Linda Strande | Barbara Jeanne Ward| SANDEC | Dewatering of faecal sludge (REACON 2) | ?
:no_entry_sign: | Eberhard Morgenorth | Nicolas Derlon  | ENG | Aerobic granular sludge (REACON 1) | 1 hr
:e-mail: | Joao Leitao | Joao Leitao | SWW | Model based flood alarm systems (REACON 0) | ~30 s
:white_check_mark: :newspaper: | Mario Schirmer | Christian Möck | WUT | Transport in Groundwater (REACON 0) | 1 hr
:white_check_mark: :newspaper: | Christoph Ort | Lena Mutzner | SWW | Model based detection of critical micropollutant discharges (REACON 0) | ~10 min
:e-mail: | Karim Abbaspour | Gokahn Cuceloglu | SIAM | Large scale water catchment and climate change analyses (REACON 1) |5-10 hr
:e-mail: | Lenny Winkel | Aryeh Feinberg | WUT | Rainfall as a potential source of selenium for soils (REACON 1) |
| [Damien Boufard](http://www.eawag.ch/en/aboutus/portrait/organisation/staff/profile/damien-bouffard/show/) | t.b.d. | SURF | Coupling Remote Lake Sensing In-situ and Models ([CORESIM](http://www.eawag.ch/en/department/surf/projects/coresim/)) (REACON 1) |
:e-mail: | Roni Penn | - | SWW | Sewer process modelling (REACON 0) |

### Project members
Status | Head name | Contact point | Department | Application (Readiness) | Runtime
-----------------|-----------------|-------------------|-----------------|---------------------------------|----------------
:newspaper: | Kris Villez | Kris Villez | ENG | Identification and Real-time control of nitrification reactors (REACON 1) |~30 s
:e-mail: | Jörg Rieckermann | Jörg Rieckermann | SWW | Real-time control of sewer networks (REACON 0) | ~10 min
:e-mail: | Henrik Nordborg | Adrian Rohner | HSR | Fluidic resonators (REACON 1) | ~5 min
:e-mail: | Michael Burkhardt | Michael Burkhardt | HSR | Facade pollutant leaching models (REACON 1) | ?

### External
Status | Head name | Contact point | Department | Application (Readiness) | Runtime
-----------------|-----------------|-------------------|-----------------|---------------------------------|----------------
:white_check_mark: :newspaper: | Vasilis Bellos (QUICS) | <mailto:vasilis.bellos@ch2m.com> | CH2M HILL | Infoworks emulation of water networks (REACON 1) | ~1 min
| Markus Gresch <mailto:markus.gresch@hunziker-betatech.ch> (INKA)| Thomas Hug <mailto:Thomas.Hug@hunziker-betatech.ch> | Kompetenz-Zentrum Hunziker Betatech AG | Design and calibration of hydraulic structures. CFD simulation of wastewater and drinking water infrastructure (REACON 1). |~1 min ~10 hrs
:white_check_mark: | Mario Mürmann (ArcSim) | <mailto:mario.muermann@hsr.ch> | HSR | Electric arc simulation (REACON 1) | ~1 day
:white_check_mark: :newspaper:| Antonio Manuel Moreno Ródenas | <mailto:a.m.morenorodenas@tudelft.nl>  | TU Delft | Water quality simulation (REACON 0) | 5-12 min
:newspaper: | Jaeger Christian <mailto:jaeg@zhaw.ch> | Bolt Peter <mailto:bolt@zhaw.ch> | ZHAW Winterthur | Heat exchange in buildings (REACON 0) | 10 min