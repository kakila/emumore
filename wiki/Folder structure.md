# EmuMore's repository folder structure

In this page:

[TOC]

## At root level

The files of the projects are organized in the following categories (the folder name is given in parentheses):

* Documents (``doc``)
    Here we put sources to all documentation generated in the project (ideally LaTex files).
    This might include working papers, agendas, minutes, etc.

* Blog development (``blog``)
    This is the folder were the development files for the blog (we are using [Jekyll](http://jekyllrb.com/)) are stored
    and where we work on the drafts and themes of the blog.$
    The deployed html is not in this folder, (_pssst..._ [it is here](https://bitbucket.org/KaKiLa/kakila.bitbucket.org/src/tip/emumore/)).

* Source code (``src``)
    This is a complicated folder.
    It contains source code for the many components of the project:

      * emulation and reduction methods, e.g. rbm
      * general scripts ,i.e. exploratory code, examples, etc. This folder can be quite wild.
      * case studies, i.e. emulation tasks that we solved to generate deliverables of the project.

The tree below shows the structure at root level

    .
    ├── blog/
    ├── doc/
    └── src/


## Blog folder

    blog/
    ├── assets/
    ├── _data/
    ├── _includes/
    ├── _layouts/
    ├── _posts/
    ├── _site/
    ├── about.md
    ├── _config.yml
    ├── Gemfile
    ├── Gemfile.lock
    └── index.md

Since this is a Jekyll development environment, one should read its [documentation](http://jekyllrb.com/docs/home/).
Essentially add files to the ``_post`` folder when we publish a new blog post.
Other edits are done to change the visual aspect or functionality of the blog.

## Source folder

We start with the following structure

    src/
    ├── case_study/
    ├── OF_pkg/
    └── scripts/

The folders ``case_study`` and ``scripts`` were described above.
The folder ``OF_pkg`` contains the [GNU Octave]() package(s) that we develop.

Octave programs in ``case_study`` and ``scripts`` use these packages, via the code

```
#!octave

pkg load <package_name>
```

This implies that the user has already installed the package ``<package_name>``.
This is done using the ``Makefile`` within each package in the ``src/OF_pkg`` folder, but we also provide the latests installers in the [Downloads](https://bitbucket.org/KaKiLa/emumore/downloads/) of the main repository.
