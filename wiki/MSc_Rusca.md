# Master thesis plan

```
Student: Sebastiano Rusca
Updated: 02/08/2017
```

## EmuMore Requirement's
* Emulation must be a core topic of the thesis

* The theme should be within _Siedlungswasserwirtschaft_

    This is to avoid complicated negotiations with the department
    in which EmuMore is embedded.
    Also because it is the field of expertise of one of the supervisors.
    Examples of what's excluded:

    - Snow cover simulations (unless they affect the water systems of a town)
    - Hydrobiology (unless the effect of sewer discharge is the focus)
    - Energy production (less is in water treatment plants or drinking water systems)

## List of themes for the thesis

This section is filled in collaboration with the student.
Any proposed theme must provide the following information:

- **Field(s) of application**
    The field that frames the theme of the thesis.
    There can be several.

- **Existing numerical models**
    Indicate (link to resource) whether numerical models that are accepted by, or popular in,
    the field already exist.
    Also indicate (link to resource) if there are available simulators implementing
    the model.
    If there is no model **it is not** a suitable theme.
    If there is no simulator available, we can consider if that can be part of the thesis (less likely to be seen as a suitable theme).

- **Specific case description**
    Give a concrete example of how the results of the thesis could be applied or used in
    the field of application.
    It doesn't mean the thesis will be about this, is just to make clear potential use of the results.

- **Co-supervisor**
    Extra expertise might be added by defining a co-supervisor.

### Emulation of inflow to drinking water reservoir
**`Field of application`**: Urban drinking water networks

**`Existing numerical models`**: [EPANET](https://www.epa.gov/water-research/epanet)

**`Specific case description`**:
Reservoir design.
A new water reservoir is to be built.
The builders want to optimize costs by matching the design of the reservoir to the behavior of the water network and the expected evolution in the next 20 years.
To perform the optimization many scenarios should be considered, the current simulators are too slow to do this.
We will develop an emulator of the inflow to the reservoir using data from the existing models.
