* Emulate CSO tank => introduce emulators in engineering practice
* Emulate pollutant transport in short sewer reach => estimate Q from water level measurements and natural tracers (Temp, EC)
* Emulate water levels in all manholes of a SWMM model => flood risk assessment in real time
*