# Resources

## Websites

* https://en.wikipedia.org/wiki/Surrogate_model

* mor.org

* gassuainprocesses.org

* http://www.mrzv.org/software/dionysus/ ([TDA](https://en.wikipedia.org/wiki/Topological_data_analysis): persistent homology), https://arxiv.org/pdf/1506.08903v4.pdf, http://people.maths.ox.ac.uk/nanda/perseus/index.html, https://github.com/appliedtopology/javaplex, 



## Software

### Multi lang

* [Shogun](http://www.shogun-toolbox.org/)

### C++

* [ML pack](http://mlpack.org/)
* [Shallow water equations and CFD with PETSc_FEM](http://www.cimec.org.ar/twiki/bin/view/Cimec/PETScFEM)

### Python
https://www.python.org/

* [pyMOR](http://pymor.org/)

  It is quite complete, but it is meant
  for especialists. We are aiming at a lower levle user with far
  less knowledge about the internals.
  We will probably write an interface to pyMOR, aimed at making it more
  accesible...

* [pyGPs](http://www-ai.cs.uni-dortmund.de/weblab/static/api_docs/pyGPs/)

* [Scikit-Learn](http://scikit-learn.org/stable/)

### GNU Octave / Matlab
https://www.gnu.org/software/octave/

* [GPML](http://gaussianprocess.org/gpml/) Gaussian Proceses for Machine Learning.

* [SUMO](http://www.sumo.intec.ugent.be/SUMO)

* [Fractional order differential equations](https://ch.mathworks.com/matlabcentral/fileexchange/22071-matrix-approach-to-discretization-of-odes-and-pdes-of-arbitrary-real-order)

### Scilab
http://scilab.io/

* Model Order redcution toolbox

### Julia
https://julialang.org/

* [GP](https://github.com/STOR-i/GaussianProcesses.jl)

* [Scikit-Learn](https://github.com/cstjean/ScikitLearn.jl)

* [JuliaML](https://juliaml.github.io/)

### R
https://www.r-project.org/