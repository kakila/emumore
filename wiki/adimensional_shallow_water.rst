======================================================
Adimensional 1D shallow water equations (Saint-Venant)
======================================================

The 1D shallow water equations, written in the conservative form, are:

.. math::
    \frac{\partial\mathcal{A}}{\partial\tau} + \frac{\partial\mathcal{Q}}{\partial\xi} & = 0\\
    \frac{\partial\mathcal{Q}}{\partial\tau} + \frac{\partial}{\partial\xi}\frac{\mathcal{Q}^2}{\mathcal{A}} + g\mathcal{A}\frac{\partial\mathcal{H}}{\partial\xi} + g \mathcal{A} (S_f - S_z) & = 0\\
    \mathcal{H}(0,\tau) &= \mathcal{H}_{\textrm{in}}(\tau)\\
    \mathcal{Q}(0,\tau) &= \mathcal{Q}_{\textrm{in}}(\tau)\\
    \mathcal{H}(\xi,0) &= \mathcal{H}_o(\xi)\\
    \mathcal{Q}(\xi,0) &= \mathcal{Q}_o(\xi)

where :math:`\mathcal{A},\mathcal{H},\mathcal{Q}` are the flow's cross-sectional area,
the water depth, and the flow of the fluid in the channel, respectively.
All these magnitudes are functions of the position along the channel :math:`\xi`, and
of time :math:`\tau`.
Since the channel is assumed to be fixed in time (e.g. no erosion, no moving walls),
and we also assume that the cross-section of the channel is constant within the domain of :math:`\xi` (i.e. the shape of the channel is constant), the cross-sectional area of the flow is actually just a function of the water depth.

The equations can be interpreted as the transformation that a channel defined in a domain :math:`\xi \in [0,L]` applies to input data :math:`\mathcal{H}_{\textrm{in}}, \mathcal{Q}_{\textrm{in}}`.

We now consider a new set of variables which are scalings of the dimensional variables described above,

.. math::
    \xi &= L x\\
    \tau &= T t\\
    \mathcal{H}(\xi,\tau) &= H h(x,t)\\
    \mathcal{A}(\mathcal{H}) &= A a(h)\\
    \mathcal{Q}(\xi,\tau) &= Q q(x,t)

where :math:`L` is the length of :math:`\xi` domain, e.g. the length of a pipe.
:math:`H` is the maximum level the fluid can attain in the domain.
:math:`A` is the total cross-sectional area of channel.
:math:`Q` and :math:`T` are free scalings to be defined.

Plugging the new variables in the equations we get a set of adimensional equations

.. math::
    \frac{\partial a}{\partial t} + \frac{\partial q}{\partial x} &= 0\\
    \frac{\partial q}{\partial t} + \frac{\partial}{\partial x}\frac{q^2}{a} + S_H a\frac{\partial h}{\partial x} + (S_f - S_z)a &= 0\\
    h(0,t) &= h_{\textrm{in}}(t)\\
    q(0,t) &= q_{\textrm{in}}(t)\\
    h(x,0) &= h_o(x)\\
    q(x,0) &= q_o(x)

which are derived by choosing the free scalings as

.. math::
    Q = \frac{LA}{T}\\
    T^2 = \frac{L}{g}

The :math:`S_*` coefficients in the adimensional equations are slopes given by

.. math::
    S_f &= f_D \frac{1}{2g} \frac{\mathcal{P}\mathcal{Q}^2}{4\mathcal{A}^3} = \frac{f_D}{8S_R} \frac{pq^2}{a^3}\\
    S_R &= \frac{A}{P}\frac{1}{L}\\
    S_z &= - \frac{\Delta z}{L}\\
    S_H &= \frac{H}{L}

The friction slope :math:`S_f` form follows the Darcy-Weisbach formula, where :math:`\mathcal{P}` is the wetted perimeter that was adimensionalized using as scaling the total perimeter of the channel :math:`P`.
The slope :math:`S_R` involves the maximum hydraulic radius, :math:`S_z` involves the change of height of the base of the channel :math:`\Delta z`, and :math:`S_H` invovles the maximum possible water depth in the channel.
The last three slopes are purely geometrical parameters defined by the shape of the channel.

The adimensionalized equations tells us that the transformation applied by a pipe to the input data is parametrized by the three slopes and the friction coefficient , i.e.

.. note:: The friction coeffcient is a function of the flow, so it is not strictly a parameter if the flow changes a lot.

.. math::
    \begin{bmatrix}\mathcal{H}\\ \mathcal{Q}\end{bmatrix}(L,\tau) = \begin{bmatrix}\mathcal{H}_{\textrm{out}} \\ \mathcal{Q}_{\textrm{out}}\end{bmatrix}(\tau) = \mathcal{F}_{f_D, S_R, S_H, S_z}\left(\begin{bmatrix}\mathcal{H}_{\textrm{in}} \\ \mathcal{Q}_{\textrm{in}}\end{bmatrix}(\tau)\right)
